<!-- App Sidebar -->
<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
<aside class="app-sidebar">
	<div class="col-lg-12" style="margin-top: 12px !important;">
		<div class="main-profile-overview">
			<div class="main-img-user profile-user">
				<img alt="" src="{{asset('assets/img/users/male/avatar.png')}}">
				<a href="JavaScript:void(0);" class="fas fa-camera profile-edit"></a>
			</div>
			<div class="justify-content-between mg-b-20 mt-2 text-center">
				<div>
					<h5 class="main-profile-name"></h5>
					<p class="main-profile-name-text text-muted">
						<div id="autoload">
							<div id="time">
								<?php echo date("h:i:sa");?>
							</div>
						</div>
						<p>
							<h6>Campaign Name:{{Session::get('campaign')['campaignname']}}</h6>
							<h6>Campaign Expaire At:{{Session::get('campaign')['campaigndescription']}}</h6>
							<h6> Total Campaign:{{Session::get('campaign')['campaignlimit']}}</h6>
						</p>
					</p>
				</div>
			</div>
			<div class="mt-2 text-center">
				<?php
				if(Session::get('campaign')['campaignlimit']==0){?>
					<a href="#" class="btn btn-danger">Payment Required</a><?php
				}else{?>
					<a href="#" class="btn btn-success">Campaign Ramaining:{{Session::get('campaign')['campaignlimit']}}</a><?php
				}?>
			</div>
		</div><!-- main-profile-overview -->
	</div>
	<ul class="side-menu">
		<li class="slide">
			<a class="side-menu__item" href="{{url('/')}}"><i class="side-menu__icon fas fa-tv"></i><span class="side-menu__label">Dashboard</span></a>
		</li>
		<li class="slide">
			<a class="side-menu__item" href="{{url('/managecampaign')}}"><i class="side-menu__icon fas fa-cog" ></i><span class="side-menu__label">Manage Campaign</span></a>
		</li>
		<li class="slide">
			<a class="side-menu__item" href="{{url('/popmail')}}"><i class="side-menu__icon fas fa-envelope-open-text"></i><span class="side-menu__label">POP Mail</span></a>
		</li>
		<li class="slide">
			<a class="side-menu__item" href="{{url('/imagemanager')}}"><i class="side-menu__icon fas fa-images"></i><span class="side-menu__label">Image Manager</span></a>
		</li>
		
		<li class="slide">
			<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fas fa-file-alt"></i><span class="side-menu__label">Replay Message</span><i class="angle fe fe-chevron-down"></i></a>
			<ul class="slide-menu">
				<li><a class="slide-item" href="{{url('/create_replay')}}">Create Replay</a></li>
				<li><a class="slide-item" href="{{url('/all_replay')}}">All Replay</a></li>
			</ul>
		</li>
		<li class="slide">
			<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fas fa-arrows-alt"></i><span class="side-menu__label">Followup Message</span><i class="angle fe fe-chevron-down"></i></a>
			<ul class="slide-menu">
				<li><a class="slide-item" href="{{url('/create_followup')}}">Create Followup</a></li>
				<li><a class="slide-item" href="{{url('/all_followup')}}">All Followup</a></li>
			</ul>
		</li>
		<?php
		if(Session::get('user')['privillage']==1000){?>
			<li class="slide">
				<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fas fa-mail-bulk"></i><span class="side-menu__label">Lead Manager</span><i class="angle fe fe-chevron-down"></i></a>
				<ul class="slide-menu">
					<li><a class="slide-item" target="_blank" href="{{url('/leadgenerate')}}">Generate Lead</a></li>
					<li><a class="slide-item" href="{{url('/manageleads')}}">Manage Lead</a></li>
				</ul>
			</li>
			<li class="slide">
				<a class="side-menu__item" href="{{url('/reset_system')}}"><i class="side-menu__icon fas fa-redo"></i><span class="side-menu__label">Reset System</span></a>
			</li>
			<li class="slide">
				<a class="side-menu__item" href="{{url('/manageuser')}}"><i class="side-menu__icon fas fa-users"></i></i><span class="side-menu__label">Manage User</span></a>
			</li>
			<li class="slide">
				<a class="side-menu__item" href="{{url('/blacklist')}}"><i class="side-menu__icon fas fa-filter"></i><span class="side-menu__label">Black List</span></a>
			</li>
			<li class="slide">
				<a class="side-menu__item" href="{{url('/mail')}}"><i class="side-menu__icon fas fa-at"></i><span class="side-menu__label">Manage Email</span></a>
			</li>
			<li class="slide">
				<a class="side-menu__item" href="{{url('/linkmanager')}}"><i class="side-menu__icon fas fa-link"></i><span class="side-menu__label">Link Manager</span></a>
			</li>
			<li class="slide">
				<a class="side-menu__item" href="{{url('/smtp')}}"><i class="side-menu__icon fas fa-link"></i><span class="side-menu__label">SMTP</span></a>
			</li>
			<?php
		}
		?>
	</ul>
</aside>
<!--/App Sidebar