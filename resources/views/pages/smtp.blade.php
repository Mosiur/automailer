@extends('master')
@section('content')
<form method="POST" action="{{url('/uploadcsv')}}" enctype="multipart/form-data">
	@csrf
	<div class="modal" id="fileupload">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content modal-content-demo">
				<div class="modal-header">
					<h6 class="modal-title">Upload Excel file(Suppoeted type .xls,.xlsx)</h6>
				</div>
				<div class="modal-body" id="linkform">
					<div class="pd-30 pd-sm-40 bg-light">
						<div class="row row-xs align-items-center mg-b-20">
							<div class="col-md-4">
								<label class="form-label mg-b-0">File:</label>
							</div>
							<div class="col-md-8 mg-t-5 mg-md-t-0">
								<input class="form-control" type="file" name="file" id="file">
							</div>
						</div>
						<div class="row row-xs align-items-center mg-b-20">
							<div class="col-md-4"></div>
							<div class="col-md-8 mg-t-5 mg-md-t-0">
								<button class="btn btn-main-primary pd-x-30 mg-r-5 mg-t-5" type="submit">Upload</button>
								<button class="btn btn-dark pd-x-30 mg-t-5" data-dismiss="modal" type="button">Cancel</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>




<!--Main Content-->
<div class="main-content px-0 app-content">
	<!--Main Content Container-->
	<div class="container-fluid pd-t-60">
		<!--Page Header-->
		<div class="page-header">
			<h3 class="page-title">SMTP</h3>
			<span class="success" style="color:green; margin-top:10px; margin-bottom: 10px;"></span>
			<span class="failed" style="color:red; margin-top:10px; margin-bottom: 10px;"></span>
			<?php
			if (session('success')){?>
				<div class="btn-success" style="color:white; text-align: center; font-size: 20px;">{{ session('success') }}</div>
				<?php
			}
			if(session('errors')){
				if(is_array(session('errors'))){
					foreach($errors as $error)?>
						<div class="btn-danger" style="color:white; text-align: center; font-size: 20px;">{{$error}}</div>
					<?php 
				}else{
					?>
					<div class="btn-warning" style="color:white; text-align: center; font-size: 20px;">{{ session('errors') }}</div>
					<?php
				}
			}
			?>
			<ol class="breadcrumb mb-0">
				<li class="breadcrumb-item"><a href="#">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">SMTP</li>
			</ol>
		</div>
	</div><!--Main Content Container-->

	<!--Main Content Container-->
	<div class="container-fluid pd-t-0">
		<!--Page Header-->
		<div class="page-header" style="margin: 0px !important;">
			<div class="col-md-2">
				<button style="border-radius: 10px; float: left; width: 100%;" id="active1" class=" btn btn-info">Group-1</button>
			</div>
			<div class="col-md-2">
				<button style="border-radius: 10px; float: left;width: 100%;" id="inactive1"class=" btn btn-warning">Inactive Group-1</button>
			</div>
			<div class="col-md-2">
				<button style="border-radius: 10px; float: left;width: 100%;" id="active2"class=" btn btn-info">Group-2</button>
			</div>
			<div class="col-md-2">
				<button style="border-radius: 10px; float: left;width: 100%;" id="inactive2"class=" btn btn-warning">Inactive Group-2</button>
			</div>
			<div class="col-md-4"></div>	
		</div>
	</div><!--Main Content Container-->

	<!--Table start-->
	<div class="container-fluid pd-t-20" id="active1_table">
		<!--Row-->
		<div class="card mg-b-20">
			<div class="card-body">
				<div class="table-responsive">
					<button id="button" class="btn btn-primary btn-sm mg-b-20" data-toggle="modal" data-target="#fileupload">Upload Excel</button>
					<a href="{{url('assets\uploads\file\sample\sample_smtp.csv')}}"><button id="button" class="btn btn-primary btn-sm mg-b-20">Download Sample file</button></a>

					<form action="{{url('/inactivegroup1')}}" method="POST">
						@csrf
						<button type="submit" id="group1inac" class="btn btn-warning btn-sm mg-b-20 text-white">Inactive</button>

						<!-- <table class="table text-md-nowrap" id="example-delete"> -->
							<table class="table text-md-nowrap" id="activetable1">
								<thead>
									<tr>
										<th><input type="checkbox" name="active1all" id="active1all"></th>
										<th>SL</th>
										<th>Email</th>
										<th>Password</th>
										<th>Hit</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
									<?php $counter=0;?>
									@foreach($group1data as $row)
									<tr>
										<td><input type="checkbox" name="<?php echo $row->id;?>"></td>
										<td><?php echo ++$counter; ?></td>
										<td>{{$row->uname}}</td>
										<td>{{$row->password}}</td>
										<td>{{$row->hits}}</td>
										<!-- <td onblur="update('{{$row->id}}')" id="{{$row->id}}"contenteditable><?php echo ($row->flag == '1')?'Active':'Inactive'; ?></td> -->
										<?php
										if($row->flag == '1'){
											?>
											<td>
												<button onclick="update('{{$row->id}}','{{$row->flag}}')" id="{{$row->id}}" class="btn btn-success" style="border-radius: 10px;">
													<?php echo "Active";?>
												</button>
											</td>
											<?php
										}else{?>
											<td>
												<button onclick="update('{{$row->id}}','{{$row->flag}}')" id="{{$row->id}}" class="btn btn-warning" style="border-radius: 10px;">
													<?php echo "Active";?>
												</button>
											</td>
											<?php
										}
										?>

									</tr>
									@endforeach
								</tbody>
							</table>
						</form>
					</div>
				</div>
			</div>
			<!--/Row -->
		</div>
		<!--table end-->

		<!--Table start-->
		<div class="container-fluid pd-t-20" id="active2_table">
			<!--Row-->
			<div class="card mg-b-20">
				<div class="card-body">
					<div class="table-responsive">
						<button id="button" class="btn btn-primary btn-sm mg-b-20" data-toggle="modal" data-target="#fileupload">Upload Excel</button>
						<a href="{{url('assets\uploads\file\sample\sample1_smtp.csv')}}"><button id="button" class="btn btn-primary btn-sm mg-b-20">Download Sample file</button></a>
						<!-- <table class="table text-md-nowrap" id="example1"> -->
							<table class="table text-md-nowrap" id="activetable2">
								<thead>
									<tr>
										<th><input type="checkbox" name="active2all" id="active2all"></th>
										<th>SL</th>
										<th>Email</th>
										<th>Password</th>
										<th>Hit</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
									<?php $counter=0;?>
									@foreach($group2data as $value)
									<tr>
										<td><input type="checkbox" name="<?php echo $value->id;?>"></td>
										<td><?php echo ++$counter; ?></td>
										<td>{{$value->uname}}</td>
										<td>{{$value->password}}</td>
										<td>{{$value->hits}}</td>
										<?php
										if($row->flag == '1'){
											?>
											<td>
												<button onclick="update('{{$value->id}}','{{$value->flag}}')" id="{{$value->id}}" class="btn btn-success" style="border-radius: 10px;">
													<?php echo "Active";?>
												</button>
											</td>
											<?php
										}else{?>
											<td>
												<button onclick="update('{{$value->id}}','{{$value->flag}}')" id="{{$value->id}}" class="btn btn-warning" style="border-radius: 10px;">
													<?php echo "Active";?>
												</button>
											</td>
											<?php
										}
										?>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!--/Row -->
			</div>
			<!--table end-->

			<!--Table start-->
			<div class="container-fluid pd-t-20" id="inactive_table1">
				<!--Row-->
				<div class="card mg-b-20">
					<div class="card-body">
						<div class="table-responsive">
							<!-- <button id="button" class="btn btn-primary btn-sm mg-b-20">Delete Multiple</button> -->
							<!-- <table class="table text-md-nowrap" id="example2"> -->
								<table class="table text-md-nowrap" id="inactivetable1">
									<thead>
										<tr>
											<!-- <th><input type="checkbox" name="select_all_inact" id="select_all_inact" value=""/></th>  -->
											<th><input type="checkbox" name="inactive1all" id="inactive1all"></th>
											<th>Group ID</th>
											<th>Email</th>
											<th>Password</th>
											<th>Host</th>
											<th>port</th>
											<th>Limit</th>
											<th>Hit</th>
											<th>Status</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php $counter=0;?>
										@foreach($inactivedata1 as $data)
										<tr>
											<!-- <td><input type="checkbox" name="inbcheck[]" id="select_all_inact" value="<?php echo $data->id;?>" /></td> -->
											<td><input type="checkbox" name="<?php echo $data->id;?>"></td>
											<td><?php echo $data->smtpgroupid; ?></td>
											<td><?php echo $data->uname; ?></td>
											<td><?php echo $data->password; ?></td>
											<td><?php echo $data->smtphost; ?></td>
											<td><?php echo $data->smtpport; ?></td>
											<td><?php echo $data->dailylimit; ?></td>
											<td><?php echo $data->hits; ?></td>
											<?php
											if($data->flag == '1'){
												?>
												<td>
													<button onclick="update('{{$data->id}}','{{$data->flag}}')" id="{{$data->id}}" class="btn btn-success" style="border-radius: 10px;">
														<?php echo "Active";?>
													</button>
												</td>
												<?php
											}else{?>
												<td>
													<button onclick="update('{{$data->id}}','{{$data->flag}}')" id="{{$data->id}}" class="btn btn-danger" style="border-radius: 10px;">
														<?php echo "Active";?>
													</button>
												</td>
												<?php
											}
											?>
											<td onclick="return confirm('Are you sure you want to delete this item?');">
												<button  onclick="delete_smtp('{{$data->id}}')" class="btn btn-warning" style="border-radius: 10px;">Delete</button>
											</td>
										</tr> 
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<!--/Row -->
				</div>
				<!--table end-->

				<!--Table start-->
				<div class="container-fluid pd-t-20" id="inactive_table2">
					<!--Row-->
					<div class="card mg-b-20">
						<div class="card-body">
							<div class="table-responsive">
								<!-- <button id="button" class="btn btn-primary btn-sm mg-b-20">Delete Multiple</button> -->
								<!-- <table class="table text-md-nowrap" id="example2"> -->
									<table class="table text-md-nowrap" id="inactivetable2">
										<thead>
											<tr>
												<th><input type="checkbox" name="inactive2all" id="inactive2all" value=""/></th> 
												<th>Group ID</th>
												<th>Email</th>
												<th>Password</th>
												<th>Host</th>
												<th>port</th>
												<th>Limit</th>
												<th>Hit</th>
												<th>Status</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											<?php $counter=0;?>
											@foreach($inactivedata2 as $data)
											<tr>
												<td><input type="checkbox" name="inbcheck[]" id="select_all_inact" value="<?php echo $data->id;?>" /></td>
												<td><?php echo $data->smtpgroupid; ?></td>
												<td><?php echo $data->uname; ?></td>
												<td><?php echo $data->password; ?></td>
												<td><?php echo $data->smtphost; ?></td>
												<td><?php echo $data->smtpport; ?></td>
												<td><?php echo $data->dailylimit; ?></td>
												<td><?php echo $data->hits; ?></td>
												<?php
												if($data->flag == '1'){
													?>
													<td>
														<button onclick="update('{{$data->id}}','{{$data->flag}}')" id="{{$data->id}}" class="btn btn-success" style="border-radius: 10px;">
															<?php echo "Active";?>
														</button>
													</td>
													<?php
												}else{?>
													<td>
														<button onclick="update('{{$data->id}}','{{$data->flag}}')" id="{{$data->id}}" class="btn btn-danger" style="border-radius: 10px;">
															<?php echo "Active";?>
														</button>
													</td>
													<?php
												}
												?>
												<td onclick="return confirm('Are you sure you want to delete this item?');">
													<button  onclick="delete_smtp('{{$data->id}}')" class="btn btn-warning" style="border-radius: 10px;">Delete</button>
												</td>
											</tr> 
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<!--/Row -->
					</div>
					<!--table end-->


				</div>
				<!--Main Content-->
				<script type="text/javascript">
					$(document).ready(function () {
						$('body').on('click', '#active1all', function () {
							if ($(this).hasClass('allChecked')) {
								$('input[type="checkbox"]', '#activetable1').prop('checked', false);
							} else {
								$('input[type="checkbox"]', '#activetable1').prop('checked', true);
							}
							$(this).toggleClass('allChecked');
						})

						$('body').on('click', '#active2all', function () {
							if ($(this).hasClass('allChecked')) {
								$('input[type="checkbox"]', '#activetable2').prop('checked', false);
							} else {
								$('input[type="checkbox"]', '#activetable2').prop('checked', true);
							}
							$(this).toggleClass('allChecked');
						})

						$('body').on('click', '#inactive1all', function () {
							if ($(this).hasClass('allChecked')) {
								$('input[type="checkbox"]', '#inactivetable1').prop('checked', false);
							} else {
								$('input[type="checkbox"]', '#inactivetable1').prop('checked', true);
							}
							$(this).toggleClass('allChecked');
						})

						$('body').on('click', '#inactive2all', function () {
							if ($(this).hasClass('allChecked')) {
								$('input[type="checkbox"]', '#activetable2').prop('checked', false);
							} else {
								$('input[type="checkbox"]', '#activetable2').prop('checked', true);
							}
							$(this).toggleClass('allChecked');
						})
					});
				</script>
				<script>
					$(document).ready(function(){
						$("#active1_table").show();
						$("#active2_table,#inactive_table1,#inactive_table2").hide();

						$("#active1").click(function(){
							$("#active1_table").show();
							$("#active2_table,#inactive_table1,#inactive_table2").hide();
						});
						$("#active2").click(function(){
							$("#active2_table").show();
							$("#active1_table,#inactive_table1,#inactive_table2").hide();
						});
						$("#inactive1").click(function(){
							$("#inactive_table1").show();
							$("#active1_table,#active2_table,#inactive_table2").hide();
						});
						$("#inactive2").click(function(){
							$("#inactive_table2").show();
							$("#active1_table,#active2_table,#inactive_table1").hide();
						});
						
					});
				</script>

				<script type="text/javascript">
					
					function update(id,flag){
		// var status=$("#"+id).html();
		var token = "{{ csrf_token() }}";
		$.ajax({
			url:'{{url('/updateact1')}}',
			method:"POST",
			data:{
				id:id,
				flag:flag,				
				"_token":token,
			},
			success:function(Response){
				// console.log(Response);
				// alert(Response.success);
				if(Response) {
					$('.success').text(Response.success);
					$('.failed').text(Response.failed);
				}
			}
		});

	}
	function delete_smtp(id){
		var token = "{{ csrf_token() }}";
		$.ajax({
			url:'{{url('/delete_smtp')}}',
			method:"POST",
			data:{
				id:id,				
				"_token":token,
			},
			success:function(Response){
				// console.log(Response);
				// alert(Response.success);
				if(Response) {
					$('.success').text(Response.success);
					$('.failed').text(Response.failed);
				}
			}
		});
	}
</script>
@endsection