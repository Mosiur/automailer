@extends('master')
@section('content')
<!-- Add lead modal -->
<form method="POST" action="{{url('/insertimage')}}" enctype="multipart/form-data">
	@csrf
	<div class="modal" id="addimage">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content modal-content-demo">
				<div class="modal-header">
					<h6 class="modal-title">Add Image</h6>
				</div>
				<div class="modal-body" id="linkform">
					<div class="pd-30 pd-sm-40 bg-light">
						<div class="row row-xs align-items-center mg-b-20">
							<div class="col-md-4">
								<label class="form-label mg-b-0">Name:</label>
							</div>
							<div class="col-md-8 mg-t-5 mg-md-t-0">
								<input  class="form-control" type="text" name="name" required placeholder="write a name of an image which is use to identify">
							</div>
						</div>
						<div class="row row-xs align-items-center mg-b-20">
							<div class="col-md-4">
								<label class="form-label mg-b-0">Image:</label>
							</div>
							<div class="col-md-8 mg-t-5 mg-md-t-0">
								<input class="form-control" type="file" name="img">
							</div>
						</div>
						<div class="row row-xs align-items-center mg-b-20">
							<div class="col-md-4"></div>
							<div class="col-md-8 mg-t-5 mg-md-t-0">
								<button class="btn btn-main-primary pd-x-30 mg-r-5 mg-t-5" type="submit">Add</button>
								<button class="btn btn-dark pd-x-30 mg-t-5" data-dismiss="modal" type="button">Cancel</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>

<form method="POST" action="{{url('/updt_image')}}" enctype="multipart/form-data">
	@csrf
	<div class="modal" id="editimage">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content tx-size-sm">

				<div class="modal-body tx-center pd-y-20 pd-x-20" id="imagedetails">
				</div>

			</div>
		</div>
	</div>
</form>
<!--Main Content-->
<div class="main-content px-0 app-content">

	<!--Main Content Container-->
	<div class="container-fluid pd-t-60">

		<!--Page Header-->
		<div class="page-header">
			<h3 class="page-title">Image Manager</h3>
			@if (session('success'))
			<div class="alert alert-success">
				{{ session('success') }}
			</div>
			@endif
			@if (session('failed'))
			<div class="alert alert-danger">
				{{ session('failed') }}
			</div>
			@endif
			<ol class="breadcrumb mb-0">
				<li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">Image Manager</li>
			</ol>
		</div>
		<!--Page Header-->

		<!-- Row -->
		<div class="row row-sm">
			<div class="col-lg-12">
				<div class="main-content-body d-flex flex-column">

					<div class="card p-4">
						<div class="main-content-label mg-b-5">
							Image List
							<a class="btn btn-info" style="float: right !important; border-radius: 10px; margin: 5px;" data-target="#addimage" data-toggle="modal" >Add
							</a>
						</div>
						<div class="table-responsive ecommerce-cart">
							<table class="table table-bordered text-nowrap">
								<thead class="text-center">
									<tr>
										<th class="wd-lg-30p wd-300">SL</th>
										<th>Images</th>
										<th>Title</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									@foreach($image_list as $value)
									<tr>
										<td>{{$value->id}}</td>
										<td>
											<div class="d-flex wd-300">
												<img src="{{$value->imagelink}}" class="mr-2 wd-60 bg-light" alt="img">
											</div>
										</td>
										<td>{{$value->name}}</td>
										<td>
											<a onclick="edit_image('{{$value->id}}')"><button class="btn btn-warning" ><i class="far fa-edit"></i>Edit</button></a>

											<a href="{{ route('delete_image', ['id' => $value->id])}}"><button class="btn btn-danger" ><i class="far fa-trash-alt"></i> Remove</button></a>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
					<!-- div -->
				</div>
			</div>
		</div>
	</div>
</div>
<!--Main Content-->
@endsection