@extends('master')
@section('content')
<!--Main Content-->
<div class="main-content px-0 app-content">
	<!--Main Content Container-->
	<div class="container-fluid pd-t-60">
		<!--Page Header-->
		<div class="page-header">
			<h3 class="page-title">Dashboard</h3>
			<ol class="breadcrumb mb-0">
				<li class="breadcrumb-item"><a href="#">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">Dashboard</li>
			</ol>
		</div>
		<!--Page Header-->
		<!--Row-->
		<!-- <div class="row row-sm">
			<div class="col-lg-4">
				<div class="card">
					<div class="card-body">
						<div class="main-content-label mg-b-5">
							Today Revenue:
						</div>
						<div class="table-responsive">
							<table class="table table-hover mb-0 text-md-nowrap">
								<thead>
									<tr>
										<th>Campaign</th>
										<th>Conversion</th>
										<th>Payout</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<?php
										//if($campaign){
											//foreach($campaign as $campaigndata):?>
												<td style="cursor: progress;">
													<?php //echo $campaigndata->campaignname; ?>
												</td>
											<?php //endforeach;
										//}?>
										<td><?php //echo $todayconv; ?></td>
										<td><?php
										//echo number_format($todaypayout, 2, '.', ',')."";
										?></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div> -->
			<!-- <div class="col-lg-4">
				<div class="card">
					<div class="card-body">
						<div class="main-content-label mg-b-5">
							Total Revenue:
						</div>
						<div class="table-responsive">
							<table class="table table-hover mb-0 text-md-nowrap">
								<thead>
									<tr>
										<th>Campaign</th>
										<th>Conversion</th>
										<th>Payout</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<?php// if($campaign){foreach($campaign as $campaigndata):?><td style="cursor: progress;"><?php// echo $campaigndata->campaignname; ?></td><?php// endforeach;}?>
										<td style="cursor: progress;"><?php// echo $totalconv; ?></td>
										<td style="cursor: progress;"><?php //echo number_format($totalpayout, 2, '.', ',')."";?></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="card">
					<div class="card-body">
						<div class="main-content-label mg-b-5">
							Leads Overview:
						</div>
						<div class="table-responsive">
							<table class="table table-hover mb-0 text-md-nowrap">
								<thead>
									<tr>
										<th>Today Leads</th>
										<th>Monthly Leads (<?php //echo date("M-Y");?>)</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><?php //echo $todayleads;?></td>
										<td><?php //echo $monthlyleads;?></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div> -->
		<!--/Row -->
	<!--</div>Main Content Container-->

	<!--Main Content Container-->
	<div class="container-fluid pd-t-20">
		<!--Row-->
		<div class="row row-sm">
			<div class="col-lg-6">
				<div class="card">
					<div class="card-body">
						<div class="main-content-label mg-b-5">
							Today Statistics:
						</div>
						<div class="table-responsive">
							<table class="table table-hover mb-0 text-md-nowrap">
								<thead>
									<tr>
										<th>Campaign</th>
										<th>Leads</th>
										<th>Reply</th>
										<th>FollowUp</th>
										<th>Complete</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<?php if($campaign){
											foreach($campaign as $campaigndata):?>
												<td>
													<?php echo $campaigndata->campaignname; ?>
												</td>
											<?php endforeach;
										}?>
										<td>
											<?php echo $todayleads;?>
										</td>
										<td>
											<?php echo ($today_reply-$today_waiting);?>
										</td>
										<td>
											<?php echo ($today_followup - $todayleads);?>
										</td>
										<td>
											<?php echo $today_complete;?>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="card">
					<div class="card-body">
						<div class="main-content-label mg-b-5">
							Total Statistics:
						</div>
						<div class="table-responsive">
							<table class="table table-hover mb-0 text-md-nowrap">
								<thead>
									<tr>
										<th>Campaign</th>
										<th>Leads</th>
										<th>Reply</th>
										<th>FollowUp</th>
										<th>Complete</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<?php if($campaign){foreach($campaign as $campaigndata):?><td><?php echo $campaigndata->campaignname; ?></td><?php endforeach;}?>
										<td><?php echo $totalleads;?></td>
										<td><?php echo ($total_reply-$today_waiting);?></td>
										<td><?php echo ($total_followup - $totalleads);?></td>
										<td><?php echo $total_complete;?></td>
									</tr>
								</tbody>
							</table>
						</div>

					</div>
				</div>
			</div>
		</div>
		<!--/Row -->
	</div>
	<!--Main Content Container-->
	<div class="container-fluid pd-t-20">
		<!--Row-->
		<div class="row row-sm">
			<div class="col-lg-6">
				<div class="card">
					<div class="card-body">
						<div class="main-content-label mg-b-5">
							Report
						</div>
						<div class="table-responsive">
							<table class="table table-hover mb-0 text-md-nowrap">
								<thead>
									<tr>
										<th>Leads</th>
										<th>Time</th>
										<th>Seq</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
									<?php
									if($latestleads){
										foreach($latestleads as $rows){?>
											<tr>
												<td>
													<?php echo $rows->fromemail."@".$rows->leadhosttype;?>
												</td>
												<td>
													<?php echo date("d-M h:i A",$rows->leadtime);?>
												</td>
												<td>
													<?php echo $rows->serial;?>
												</td>
												<td>
													<?php
													$status = $rows->status;
													if($status){
														echo "<i class=\"align-middle mr-2 fas fa-fw fa-check\"></i>";
													}else{
														echo "<i class=\"align-middle mr-2 fas fa-fw fa-spinner fa-spin\"></i>";
													}?>	
												</td>
											</tr>
											<?php
										}
									}?>
								</tbody>
							</table>
							<?php if(Session::get('user')['privillage']==2||Session::get('user')['privillage'] == 1000){?>
								<a href="{{url('/manageleads')}}" class="btn btn-primary pull-right">Details Report </a>
							<?php }?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="card">
					<div class="card-body">
						<div class="main-content-label mg-b-5">
							Reply Conversation Statistics:
						</div>
						<div class="table-responsive">
							<table class="table table-hover mb-0 text-md-nowrap">
								<thead>
									<tr>
										<th>Campaign</th>
										<th>Reply Order</th>
										<th>Total Conversation</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<?php if($campaign){foreach($campaign as $campaigndata):?><td><?php echo $campaigndata->campaignname; ?></th><?php endforeach;}?>
										<td>1st</td>
										<td><?php echo $total_firstreply;?></td>
									</tr>
									<tr>
										<?php if($campaign){foreach($campaign as $campaigndata):?><td><?php echo $campaigndata->campaignname; ?></th><?php endforeach;}?>
										<td>2nd</td>
										<td><?php echo $total_secondreply;?></td>
									</tr>
									<tr>
										<?php if($campaign){foreach($campaign as $campaigndata):?><td><?php echo $campaigndata->campaignname; ?></th><?php endforeach;}?>
										<td>3rd</td>
										<td><?php echo $total_3rdreply;?></td>
									</tr>
									<tr>
										<?php if($campaign){foreach($campaign as $campaigndata):?><td><?php echo $campaigndata->campaignname; ?></th><?php endforeach;}?>
										<td>4th</td>
										<td><?php echo $total_4threply;?></td>
									</tr>
									<tr>
										<?php if($campaign){foreach($campaign as $campaigndata):?><td><?php echo $campaigndata->campaignname; ?></th><?php endforeach;}?>
										<td>5th</td>
										<td><?php echo $total_5threply;?></td>
									</tr>
									<tr>
										<?php if($campaign){foreach($campaign as $campaigndata):?><td><?php echo $campaigndata->campaignname; ?></th><?php endforeach;}?>
										<td>6th</td>
										<td><?php echo $total_6threply;?></td>
									</tr>
									<tr>
										<?php if($campaign){foreach($campaign as $campaigndata):?><td><?php echo $campaigndata->campaignname; ?></th><?php endforeach;}?>
										<td>7th</td>
										<td><?php echo $total_7threply;?></td>
									</tr>
									<tr>
										<?php if($campaign){foreach($campaign as $campaigndata):?><td><?php echo $campaigndata->campaignname; ?></th><?php endforeach;}?>
										<td>8th</td>
										<td><?php echo $total_8threply;?></td>
									</tr>
									<tr>
										<?php if($campaign){foreach($campaign as $campaigndata):?><td><?php echo $campaigndata->campaignname; ?></th><?php endforeach;}?>
										<td>9th</td>
										<td><?php echo $total_9threply;?></td>
									</tr>
									<tr>
										<?php if($campaign){foreach($campaign as $campaigndata):?><td><?php echo $campaigndata->campaignname; ?></th><?php endforeach;}?>
										<td>10th</td>
										<td><?php echo $total_10threply;?></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/Row -->
	</div>
	<!--Main Content Container-->
</div>
<!--Main Content-->
@endsection