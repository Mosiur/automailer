@extends('master')
@section('content')
<!-- Add lead modal -->
<form method="POST" action="{{url('/insertleads')}}">
	@csrf
	<div class="modal" id="addlead">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content modal-content-demo">
				<div class="modal-header">
					<h6 class="modal-title">Add Lead</h6>
				</div>
				<div class="modal-body" id="leadform">
					<div class="pd-30 pd-sm-40 bg-light">
						<div class="row row-xs align-items-center mg-b-20">
							<div class="col-md-4">
								<label class="form-label mg-b-0">Leads</label>
							</div>
							<div class="col-md-8 mg-t-5 mg-md-t-0">
								<textarea class="form-control" placeholder="Textarea" rows="3" name="leads" required></textarea>
							</div>
						</div>
						<div class="row row-xs align-items-center mg-b-20">
							<div class="col-md-4"></div>
							<div class="col-md-8 mg-t-5 mg-md-t-0">
								<button class="btn btn-main-primary pd-x-30 mg-r-5 mg-t-5" type="submit">Add Lead</button>
								<button class="btn btn-dark pd-x-30 mg-t-5" data-dismiss="modal" type="button">Cancel</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>

<form method="POST" action="{{url('/delete_leads')}}">
	@csrf
	<div class="modal" id="deletelead">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content modal-content-demo">
				<div class="modal-header">
					<h6 class="modal-title">Delete Leads</h6>
				</div>
				<div class="modal-body">
					<div class="row row-xs align-items-center mg-b-20">
						<div class="col-md-4">
							<label class="form-label mg-b-0">Start Date:</label>
						</div>
						<div class="col-md-8 mg-t-5 mg-md-t-0">
							<input class="form-control" placeholder="Enter your password" type="date" name="srtdate" required>
						</div>
					</div>
					<div class="row row-xs align-items-center mg-b-20">
						<div class="col-md-4">
							<label class="form-label mg-b-0">End date:</label>
						</div>
						<div class="col-md-8 mg-t-5 mg-md-t-0">
							<input class="form-control" placeholder="Enter your password" type="date" name="enddate" required>
						</div>
					</div>
					<div class="row row-xs align-items-center mg-b-20">
						<div class="col-md-4"></div>
						<div class="col-md-8 mg-t-5 mg-md-t-0">
							<button class="btn btn-main-primary pd-x-30 mg-r-5 mg-t-5" type="submit" onclick="confirm('Are you sure to delete leads...?')">Delete</button>
							<button class="btn btn-dark pd-x-30 mg-t-5" data-dismiss="modal" type="button">Cancel</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
<!--Main Content-->
<div class="main-content px-0 app-content">
	<!--Main Content Container-->
	<div class="container-fluid pd-t-60">
		<!--Page Header-->
		<div class="page-header">
			<h3 class="page-title">Lead list</h3>
			@if (session('success'))
			<div class="alert alert-success">
				{{ session('success') }}
			</div>
			@endif
			@if (session('failed'))
			<div class="alert alert-danger">
				{{ session('failed') }}
			</div>
			@endif
			<ol class="breadcrumb mb-0">
				<li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a></li>
				<li class="breadcrumb-item active" aria-current="page">Lead list</li>
			</ol>
		</div>
		<!--Page Header-->
		<!--Row-->
		<div class="row row-sm">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<div class="main-content-label mg-b-5">
							Leads

							<a class="btn btn-info" style="float: right !important; border-radius: 25px; margin: 5px;" data-target="#addlead" data-toggle="modal" >Add
							</a>
							<a class="btn btn-warning" style="float: right !important; border-radius: 25px;margin: 5px;" data-target="#deletelead" data-toggle="modal">Delete
							</a>
						</div>
						<div class="">
							<div class="table-responsive">
								<table id="example" class="table key-buttons text-md-nowrap" style="width: 100% !important;">
									<thead>
										<tr>
											<th class="border-bottom-0">SL</th>
											<th class="border-bottom-0">Leads Mail</th>
											<th class="border-bottom-0">From Name</th>
											<th class="border-bottom-0">Time</th>
											<th class="border-bottom-0">Sequence</th>
											<th class="border-bottom-0">Status</th>
											<th class="border-bottom-0">Followup</th>
										</tr>
									</thead>
									<tbody>
										@foreach($leadsdata as $leaddata)
										<tr>
											<td><?php echo $leaddata->id;?></td>
											<td><?php echo $leaddata->fromemail."@".$leaddata->leadhosttype;?></td>
											<td><?php echo strip_tags(htmlspecialchars_decode($leaddata->fromname));?></td>
											<td><?php echo date("d-M H:i A",$leaddata->leadtime);?></td>
											<td><?php echo $leaddata->serial;?></td>
											<td><?php $status = $leaddata->status; if($status){ echo "<i class=\"align-middle mr-2 fas fa-fw fa-check\"></i>";}else{ echo "<i class=\"align-middle mr-2 fas fa-fw fa-spinner fa-spin\"></i>";}?></td>
											<td><?php echo $leaddata->followup;?></td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--/Row -->

</div>
</div>

<!--Main Content-->
@endsection