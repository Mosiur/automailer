@extends('master')
@section('content')
<!--Main Content-->
<div class="main-content px-0 app-content">

	<!--Main Content Container-->
	<div class="container-fluid pd-t-60">

		<!--Page Header-->
		<div class="page-header">
			<h3 class="page-title">Update Followup Message</h3>
			<ol class="breadcrumb mb-0">
				<li class="breadcrumb-item"><a href="{{url('/')}}">Pages</a></li>
				<li class="breadcrumb-item active" aria-current="page">Edit followup Message</li>
			</ol>
		</div>
		<!--Page Header-->

		<div class="row">
			<div class="col-lg-12">
				<div class="main-content-body d-flex flex-column">

					<div class="card p-4">
						<!-- Row -->
						<div class="card-body">
							<div class="main-content-label mg-b-5">
								Edit followup
							</div>
							<div class="pd-30 pd-sm-40 bg-light">
								@foreach($followupdata as $value)
								<form action="{{route('updatefollowup',['id'=>$value->id])}}" method="post">
									@csrf
									
									<div class="row row-xs align-items-center mg-b-20">
										<div class="col-md-3">
											<label class="form-label mg-b-0">Order By:</label>
										</div>
										<div class="col-md-9 mg-t-5 mg-md-t-0">
											<select class="form-control select2-no-search" name="replynumber" required="required">
												<option <?php if($value->replynumber == 1){ echo "selected";}?> value="1">1</option>
												<option <?php if($value->replynumber == 2){ echo "selected";}?>value="2">2</option>
												<option <?php if($value->replynumber == 3){ echo "selected";}?>value="3">3</option>
												<option <?php if($value->replynumber == 4){ echo "selected";}?>value="4">4</option>
												<option <?php if($value->replynumber == 5){ echo "selected";}?>value="5">5</option>
												<option <?php if($value->replynumber == 6){ echo "selected";}?>value="6">6</option>
											</select>
										</div>
									</div>

									<div class="row row-xs align-items-center mg-b-20">
										<div class="col-md-3">
											<label class="form-label mg-b-0">Delay time (min):</label>
										</div>
										<div class="col-md-9 mg-t-5 mg-md-t-0">
											<input name="replyinterval" class="form-control" placeholder="Enter delay time for send message" type="Number" value="<?php echo $value->replyinterval;?>" required="required" min="1">
										</div>
									</div>
									<div class="row row-xs align-items-center mg-b-20">
										<div class="col-md-3">
											<label class="form-label mg-b-0">Messsage:</label>
										</div>
										<div class="col-md-9 mg-t-5 mg-md-t-0">
											<textarea class="ckeditor" name="messagebody"><?php echo strip_tags(htmlspecialchars_decode($value->messagebody))?></textarea>
										</div>
									</div>
									@endforeach
									<div class="row row-xs align-items-center mg-b-20">
										<div class="col-md-3">
											<label class="form-label mg-b-0"></label>
										</div>
										<div class="col-md-9 mg-t-5 mg-md-t-0">
											<button class="btn btn-main-primary pd-x-30 mg-r-5 mg-t-5" type="submit">Save Changes</button>
										</div>
									</div>
								</form>
							</div>
						</div>
						<!--/Row-->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--Main Content-->
@endsection