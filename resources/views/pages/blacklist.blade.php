@extends('master')
@section('content')
<!-- edit replay message modal -->
<form method="POST" action="{{url('/addblacklist')}}">
	@csrf
	<div class="modal" id="blacklist">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content modal-content-demo">
				<div class="modal-header">
					<h6 class="modal-title">Add to Blacklist</h6>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="message-text" class="col-form-label">Keyword:</label>
						<textarea class="form-control" name="matchword" placeholder="example:noreplay"></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-indigo" type="submit" name="submit">Add Blacklist</button> <button class="btn btn-outline-light" data-dismiss="modal" type="button">Close</button>
				</div>
			</div>
		</div>
	</div>
</form>


<form method="POST" action="{{url('/updateblacklist')}}">
	@csrf
	<div class="modal" id="upblacklist">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content modal-content-demo">
				<div class="modal-header">
					<h6 class="modal-title">Update Blacklist</h6>
				</div>
				<div class="modal-body" id="blacklistdetails">
				</div>
				<div class="modal-footer">
					<button class="btn btn-indigo" type="submit">Save Changes</button> <button class="btn btn-outline-light" data-dismiss="modal" type="button">Close</button>
				</div>
			</div>
		</div>
	</div>
</form>
<!--Main Content-->
<div class="main-content px-0 app-content">
	<!--Main Content Container-->
	<div class="container-fluid pd-t-60">
		<!--Page Header-->
		<div class="page-header">
			<h3 class="page-title">Blacklist</h3>
			@if (session('success'))
			<div class="alert alert-success">
				{{ session('success') }}
			</div>
			@endif
			@if (session('failed'))
			<div class="alert alert-danger">
				{{ session('failed') }}
			</div>
			@endif
			<ol class="breadcrumb mb-0">
				<li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a></li>
				<li class="breadcrumb-item active" aria-current="page">Blacklist</li>
			</ol>
		</div>
		<!--Page Header-->
		<!--Row-->
		<div class="row row-sm">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<div class="main-content-label mg-b-5">
							Blacklist
							<a style="float: right !important;" data-toggle="modal" data-target="#blacklist"><button class="btn btn-info">Add to blacklist</button></a>
						</div>
						<div class="table-responsive">
							<table class="table table-hover mb-0 text-md-nowrap">
								<thead>
									<tr>
										<th>KeyWord</th>
										<th>Edit</th>
										<th>Delete</th>
									</tr>
								</thead>
								<tbody>
									@foreach($listdata as $row)
									<tr>
										<td>{{$row->matchword}}</td>
										<td>
											<a class="btn btn-info" style="border-radius: 25px;" onclick="edit_blacklist('{{$row->id}}')">Edit
											</a>
										</td>
										<td>
											<a class="btn btn-warning" style="border-radius: 25px;"  href="{{ route('delete_blacklist', ['id' => $row->id])}}">Delete</a>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/Row -->

	</div>
</div>
<!--Main Content-->
@endsection