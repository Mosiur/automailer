@extends('master')
@section('content')
<!-- Add lead modal -->
<form method="POST" action="{{url('/insertlink')}}">
	@csrf
	<div class="modal" id="addlink">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content modal-content-demo">
				<div class="modal-header">
					<h6 class="modal-title">Add Link</h6>
				</div>
				<div class="modal-body" id="linkform">
					<div class="pd-30 pd-sm-40 bg-light">
						<div class="row row-xs align-items-center mg-b-20">
							<div class="col-md-4">
								<label class="form-label mg-b-0">Link:</label>
							</div>
							<div class="col-md-8 mg-t-5 mg-md-t-0">
								<input  class="form-control" type="text" name="link" required>
							</div>
						</div>
						<div class="row row-xs align-items-center mg-b-20">
							<div class="col-md-4">
								<label class="form-label mg-b-0">token:</label>
							</div>
							<div class="col-md-8 mg-t-5 mg-md-t-0">
								<input  class="form-control" type="text" name="token" required>
							</div>
						</div>
						<div class="row row-xs align-items-center mg-b-20">
							<div class="col-md-4"></div>
							<div class="col-md-8 mg-t-5 mg-md-t-0">
								<button class="btn btn-main-primary pd-x-30 mg-r-5 mg-t-5" type="submit">Add</button>
								<button class="btn btn-dark pd-x-30 mg-t-5" data-dismiss="modal" type="button">Cancel</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>

<form method="POST" action="{{url('/update_link')}}">
	@csrf
	<div class="modal" id="edit_link">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content modal-content-demo">
				<div class="modal-header">
					<h6 class="modal-title">Edit link</h6>
				</div>
				<div class="modal-body" id="link_details">
				</div>
			</div>
		</div>
	</div>
</form>
<!--Main Content-->
<div class="main-content px-0 app-content">
	<!--Main Content Container-->
	<div class="container-fluid pd-t-60">
		<!--Page Header-->
		<div class="page-header">
			<h3 class="page-title">Link list</h3>
			@if (session('success'))
			<div class="alert alert-success">
				{{ session('success') }}
			</div>
			@endif
			@if (session('failed'))
			<div class="alert alert-danger">
				{{ session('failed') }}
			</div>
			@endif
			<ol class="breadcrumb mb-0">
				<li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a></li>
				<li class="breadcrumb-item active" aria-current="page">Link list</li>
			</ol>
		</div>
		<!--Page Header-->
		<!--Row-->
		<div class="row row-sm">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<div class="main-content-label mg-b-5">
							Link List
							<a class="btn btn-info" style="float: right !important; border-radius: 10px; margin: 5px;" data-target="#addlink" data-toggle="modal" >Add
							</a>
						</div>
						<div class="table-responsive">
							<table class="table table-hover mb-0 text-md-nowrap">
								<thead>
									<tr>
										<th>SL</th>
										<th>Link</th>
										<th>Token</th>
										<th style="float: right !important;">Action</th>
									</tr>
								</thead>
								<tbody>
									<?php $i=0;?>
									@foreach($link_list as $row)
									<?php $i++;?>
									<tr>
										<td><?php echo $i?></td>
										<td>{{$row->link}}</td>
										<td>{{$row->token}}</td>
										<td style="float: right !important;">
											<a class="btn btn-info" style="border-radius: 10px;" onclick="edit_link('{{$row->id}}')">Edit
											</a>
											<a class="btn btn-warning" style="border-radius: 10px;"  href="{{ route('delete_link', ['id' => $row->id])}}">Delete</a>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--/Row -->

</div>
</div>

<!--Main Content-->
@endsection