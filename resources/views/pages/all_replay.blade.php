@extends('master')
@section('content')
<!-- edit replay message modal -->
<form method="POST" action="{{url('/updt_replaymsg')}}">
	@csrf
	<div class="modal" id="editmsg">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content modal-content-demo">
				<div class="modal-header">
					<h6 class="modal-title">Update Message</h6><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body" id="msgdetails">
				</div>
				<div class="modal-footer">
					<button class="btn btn-indigo" type="submit" name="submit">Save changes</button> <button class="btn btn-outline-light" data-dismiss="modal" type="button">Close</button>
				</div>
			</div>
		</div>
	</div>
</form>
<!--Main Content-->
<div class="main-content px-0 app-content">

	<!--Main Content Container-->
	<div class="container-fluid pd-t-60">

		<!--Page Header-->
		<div class="page-header">
			<h3 class="page-title">All Replay</h3>
			@if (session('success'))
			<div class="alert alert-success">
				{{ session('success') }}
			</div>
			@endif
			@if (session('failed'))
			<div class="alert alert-danger">
				{{ session('failed') }}
			</div>
			@endif
			<ol class="breadcrumb mb-0">
				<li class="breadcrumb-item"><a href="{{url('/')}}">Pages</a></li>
				<li class="breadcrumb-item active" aria-current="page">All Replay</li>
			</ol>
		</div>
		<!--Page Header-->
		<!--Row-->
		<div class="row row-sm">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<div class="main-content-label mg-b-5">
							All Replay
						</div>
						<div class="table-responsive">
							<table class="table table-hover mb-0 text-md-nowrap">
								<thead>
									<tr>
										<th>Order</th>
										<th>SMTP</th>
										<th>Interval</th>
										<th>Subject</th>
										<th>Message Contents</th>
										<th>Atttachment</th>
										<th>Edit</th>
										<th>Delete</th>
									</tr>
								</thead>
								<tbody>
									@foreach($msgdata as $row)
									<tr>
										<td>{{$row->replynumber}}</td>
										<td>{{$row->groupname}}</td>
										<td>{{$row->replyinterval}}</td>
										<td>{{$row->subject}}</td>
										<td>{!!$row->messagebody!!}</td>
										<td></td>
										<!-- <td>
											<a class="btn btn-info" style="border-radius: 25px;" onclick="edit_replaymessage('{{$row->id}}')">EBM
											</a>
										</td> -->
										<td>
											<a class="btn btn-info" style="border-radius: 25px;" href="{{ route('edit_msq', ['id' => $row->id])}}">Edit
											</a>
										</td>
										<td>
											<a class="btn btn-warning" style="border-radius: 25px;"  href="{{ route('delete_msq', ['id' => $row->id])}}">Delete</a>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/Row -->

	</div>
</div>
<!--Main Content-->
@endsection