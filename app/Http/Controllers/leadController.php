<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;

class leadController extends Controller
{
	public function manageleads(){
		$campaignid=Session::get('campaign')['campaignid'];
		$data=DB::table('leads')
		->join('grouptbl','grouptbl.id' ,'=','leads.undergroup')
		->join('campign','campign.id','=','leads.campaignid')
		->select('leads.id', 'leads.campaignid','leads.mastermail','leads.fromemail','leads.leadhosttype','leads.fromname','leads.subject', 'leads.undergroup' , 'leads.serial' ,  'leads.followup' ,'leads.leadtime', 'leads.status', 'leads.eopen','grouptbl.groupname' , 'campign.campaignname')
		->where('leads.campaignid','=',$campaignid)
		->get();
		return view('pages.manageleads',['leadsdata'=>$data]);
	}
	public function insertleads(Request $request){
		$campaignid=Session::get('campaign')['campaignid'];
		$leads=$request->input('leads');
		$time =  time();
		$leads_array = explode(",", $leads);
		foreach ($leads_array as $leads) {
			$str = explode("@", $leads);
			$fromemail = $str[0];
			if(array_key_exists(1, $str)){
				$leadhosttype = $str[1];
			}else{
				$leadhosttype = NULL;
			}
			$affeted = DB::table('leads')->insert(['fromemail'=>$fromemail,'leadhosttype'=>$leadhosttype,'campaignid'=>$campaignid,'Msgno'=>1,'toaddress'=>'','undergroup'=>1,'leadtime'=>$time,'serial'=>1,'status'=>0,'flag'=>1,
		]);
			if($affeted){
				$status="Lead generated successfully....";
			}
		}
		if($affeted != NULL){
			return redirect('/manageleads')->with('success', 'Lead generated successfully....');
		}else{
			return redirect('/manageleads')->with('failed', 'Faild to generate Lead....');
		}

	}
	public function delete_leads(Request $request){
		$campaignid=Session::get('campaign')['campaignid'];
		$srtdate=strtotime($request->input('srtdate'));
		$enddate=strtotime($request->input('enddate'));
		$affected=DB::table('leads')->where([['leadtime','>=',$srtdate],['leadtime','<=',$enddate],['campaignid','=',$campaignid]])->delete();


		// dd($affected);
		if($affected){
			return redirect('/manageleads')->with('success', 'Record deleted successfully!');
		}else{
			return redirect('/manageleads')->with('failed', 'Failed to delete..!');
		}

	}



	public function leadgenerate(){
		$campaignid=Session::get('campaign')['campaignid'];
		$activemail=DB::table('mastermail')->where([['campaignid','=',$campaignid],['flag','=',1]])->get();
		foreach($activemail as $mastermaildata){
			$mastermailid 	= $mastermaildata->id;
			$campaignid 	= $mastermaildata->campaignid;
			$emailid 		= $mastermaildata->emailid;
			$emailpass 		= $mastermaildata->emailpass;
			$emailhost 		= $mastermaildata->emailhost;
			$emailport 		= $mastermaildata->emailport;
			$protocol 		= $mastermaildata->protocol;

			if ($protocol == 1) {
				$proto = "ssl";
			} elseif ($protocol == 2) {
				$proto = "tls";
			} else {
				$proto = "AUTH";
			}


			$hostname = '{'.$emailhost.':'.$emailport.'/imap/'.$proto.'}INBOX';
			$inbox = imap_open($hostname,$emailid,$emailpass)or die('Cannot connect to Gmail: ' . imap_last_error());
			
			error_reporting(0);			
			$emails = imap_search($inbox,'UNSEEN');
			$check = imap_mailboxmsginfo($inbox );

			
			
			if($emails){

				foreach($emails as $email_number) {

					$getheader = imap_headerinfo($inbox, $email_number);  
					$overview = imap_fetch_overview($inbox,$email_number,0);
					$header = imap_header($inbox,$email_number); 

					//@$body = imap_body($inbox, $email_number);
					

					
					//pd($getheader);	
					
					foreach($getheader->from as $row){
						$fromaddress = $row->personal;
						$fromemail = $row->mailbox;
						$leadhosttype = $row->host;


					}
					$altbdy=$leadhosttype;
					if($altbdy==gmail.com){
						$altbdy = "<div class=\"gmail_extra\"><br><div class=\"gmail_quote\">";
					}elseif(yahoo.com){
						$altbdy = "<div class=\"gmail_extra\"><br><div class=\"gmail_quote\">";
					}else{
						$altbdy = "<div class=\"gmail_extra\"><br><div class=\"gmail_quote\">";
					}
					
					@$body = trim( utf8_encode( quoted_printable_decode(imap_fetchbody($inbox, $email_number,2))));	

					$newbody = explode("Content-Disposition: attachment ", $body);

						//pd($newbody);
					$body=$altbdy;
					$body.= On ;
					$body.=date(" D, j M Y") ;
					$body.=" at ".date("h:i A").',' ;
					$body.=" $fromaddress <span dir=\"ltr\"><<a href=\"mailto:$fromemail@$leadhosttype\" target=\"_blank\">$fromemail@$leadhosttype</a>></span> wrote:" ;
					$body.= "<br><br><blockquote class=\"gmail_quote\" style=\"margin:0 0 0 .8ex;border-left:1px #ccc solid;padding-left:1ex\">" ;
					$body.= addslashes($newbody[0]);
					$body.= "</blockquote></div><br></div>" ;

					$body=trim(preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 ]/', ' ', urldecode(html_entity_decode(strip_tags($body))))));


					
					//Only for backpage
					if(strpos($leadhosttype,"backpage")){
						$str = explode("@",$getheader->reply_toaddress);
						$fromemail = $str[0];
						$leadhosttype = $str[1];
					}


					$subject 				=  addslashes($getheader->subject);
					$message_id				=  $getheader->message_id;
					$toaddress 				=  $getheader->toaddress;
					//$reply_toaddress 		=  $getheader->reply_toaddress;


					$udate		 	=     $header->udate; 
					$Msgno 			=     $header->Msgno; 		
					
					
					//Check Blacklist and delete
					$checkmatch=DB::table('blacklist')->where('matchword','=',$fromemail)->orWhere('matchword','=',$leadhosttype)->count();
					if($checkmatch > 0){
						imap_delete($inbox, $Msgno);
						imap_expunge($inbox);

					}else{
						$time = time();

						$existinglead = DB::table('leads')->select('fromemail')->where('fromemail','=',$fromemail)->count();

						if($existinglead > 0){


							$undegroup  = DB::table('leads')->select('undergroup')->where('fromemail','=',$fromemail)->get();
							foreach ($undegroup as $row) {
								$serial = $row->undergroup;
							}



							$duplicate = DB::table('leads')->select('id')->where([['fromemail','=',$fromemail],['status','=',0]])->count();

							if($duplicate > 0){

								$overflowisnotuniq  = DB::table('overflow')->select('id')->where('fromemail','=',$fromemail)->count();

								if($overflowisnotuniq > 0){

									$overflowleadserial =DB::table('overflow')->select('serial')->where('fromemail','=',$fromemail)->get();

									foreach ($overflowleadserial as $key => $value) {
										$overflowleadserial1 = $value->serial;
									}

									$overflowserial     = $overflowleadserial1+1;

									$overflowoverflow = DB::table('overflow')->select('fromemail')->where([['fromemail','=',$fromemail],['status','=',0]])->count();

									if($overflowoverflow > 0){
													///no action//
									}else{

										$affected=DB::table('overflow')->where('fromemail','=',$fromemail)->update(['campaignid'=>$campaignid,'toaddress'=>$toaddress,'mastermail'=>$emailid,'fromname'=>$fromaddress,'undergroup'=>$undegroup,'leadhosttype'=>$leadhosttype,'subject'=>$subject,'msgbody'=>$body,'Msgno'=>$Msgno,'udate'=>$udate,'message_id'=>$message_id,'leadtime'=>$time,'serial'=>$overflowserial,'followup'=>0,'overflow'=>0,'status'=>0,'flag'=>1]);

										if($affected){
											echo " Overflow message Inserted successsfully..";
										}else{
											echo "Overflow message Can not  inserted ..";
										}

									}

								}else{

									$affected=DB::table('overflow')->insert([
										'campaignid'=>$campaignid,
										'toaddress'=>$toaddress,
										'mastermail'=>$emailid,
										'fromname'=>$fromaddress,
										'fromemail'=>$fromemail,
										'leadhosttype'=>$leadhosttype,
										'subject'=>$subject,
										'msgbody'=>$body,
										'Msgno'=>$Msgno,
										'udate'=>$udate,
										'message_id'=>$message_id,
										'leadtime'=>$time,
										'serial'=>1,
										'followup'=>0,
										'overflow'=>0,
										'status'=>0,
										'flag'=>1

									]);
									if ($affected) {
										echo " Unique Overflow message inserted successfully";
									}else{
										echo "failed to insert overflow message...";
									}


									if ($leadhosttype == 'gmail.com') {
										$hosttype = 1;
									} elseif ($leadhosttype == 'yahoo.com') {
										$hosttype = 2;
									} elseif ($leadhosttype == 'ymail.com') {
										$hosttype = 2;
									} elseif ($leadhosttype == 'outlook.com') {
										$hosttype = 3;
									} elseif ($leadhosttype == 'hotmail.com') {
										$hosttype = 3;
									} elseif ($leadhosttype == 'live.com') {
										$hosttype = 3;
									} elseif ($leadhosttype == 'aol.com') {
										$hosttype = 4;
									} else {
										$hosttype = 5;
									}

									$minhits=DB::table('grouptbl')->where([['hosttype','=',$hosttype],['campaignid','=',$campaignid]])->min('hits');

									$groupdata=DB::table('replymessage')
									->join('grouptbl','grouptbl.id' ,'=','replymessage.messagegroup')
									->select('grouptbl.id', 'grouptbl.hits','replymessage.messagegroup')
									->where([['grouptbl.hosttype','=',$hosttype],['grouptbl.hits','=',$minhits],['replymessage.messagegroup','!=',NULL]])
									->orderBy('grouptbl.id')
									->first();

									if($groupdata != NULL){
										$groupid = $groupdata->id;
									}else{
										$data=DB::table('grouptbl')->select('id')->where('hosttype','=',5)->get();
										foreach ($data as $key => $value) {
											$groupid =$value->id;
										}
									}



									$result=DB::table('overflow')->where('fromemail','=',$fromemail)->update(['undergroup'=>$groupid]);
									if ($result) {
										echo "undergroup updated for overflow...";
									}else{
										echo " falied to update undergroup for overflow..";
									}


									$hits2=DB::table('grouptbl')->select('hits')->where('id','=',$groupid)->get();
									foreach ($hits2 as $key => $row) {
										$newhits2=($row->hits+1);
									}
									

									$result1=DB::table('grouptbl')->where('id','=',$groupid)->update(['hits'=>$newhits2]);
									if ($result1) {
										echo " hits are updated for group table...<br>";
									}else{
										echo "hits can not be updated for group table...<br>";
									}

								}

							}else{
								$seraialdata=DB::table('leads')->select('serial')->where('fromemail','=',$fromemail)->get();
								foreach ($seraialdata as $key => $value) {
									$leadserial = $value->serial;
								}
								$serial     = $leadserial+1;

								$data=DB::table('leads')->where('fromemail','=',$fromemail)->update(['campaignid'=>$campaignid,'toaddress'=>$toaddress,'mastermail'=>$emailid,'fromname'=>$fromaddress,'undergroup'=>$undegroup,'leadhosttype'=>$leadhosttype,'subject'=>$subject,'msgbody'=>$body,'Msgno'=>$Msgno,'udate'=>$udate,'message_id'=>$message_id,'leadtime'=>$time,'serial'=>$serial,'overflow'=>0,'status'=>0,'flag'=>1]);

								if($data){
									echo "Unique Lead inserted successfully<br>";
								}else{
									echo "failed to create lead....<br>";
								}

							}		
						}else{
							$result3=DB::table('leads')->insert([
								'campaignid'=>$campaignid,
								'toaddress'=>$toaddress,
								'mastermail'=>$emailid,
								'fromname'=>$fromaddress,
								'fromemail'=>$fromemail,
								'leadhosttype'=>$leadhosttype,
								'subject'=>$subject,
								'msgbody'=>$body,
								'Msgno'=>$Msgno,
								'udate'=>$udate,
								'message_id'=>$message_id,
								'leadtime'=>$time,
								'fleadtime'=>$time,
								'serial'=>1,
								'followup'=>1,
								'overflow'=>0,
								'status'=>0,
								'fstatus'=>1,
								'flag'=>1
							]);

							if ($result3) {
								echo "new lead inserted successfully...<br>";
							}else{
								echo "failed to insert new lead....<br>";
							}

							if ($leadhosttype == 'gmail.com') {
								$hosttype = 1;
							} elseif ($leadhosttype == 'yahoo.com') {
								$hosttype = 2;
							} elseif ($leadhosttype == 'ymail.com') {
								$hosttype = 2;
							} elseif ($leadhosttype == 'outlook.com') {
								$hosttype = 3;
							} elseif ($leadhosttype == 'hotmail.com') {
								$hosttype = 3;
							} elseif ($leadhosttype == 'live.com') {
								$hosttype = 3;
							} elseif ($leadhosttype == 'aol.com') {
								$hosttype = 4;
							} else {
								$hosttype = 5;
							}

							$minhits=DB::table('grouptbl')->where([['hosttype','=',$hosttype],['campaignid','=',$campaignid]])->min('hits');

							$groupdata=DB::table('replymessage')
							->join('grouptbl','grouptbl.id' ,'=','replymessage.messagegroup')
							->select('grouptbl.id', 'grouptbl.hits','replymessage.messagegroup')
							->where([['grouptbl.hosttype','=',$hosttype],['grouptbl.hits','=',$minhits],['replymessage.messagegroup','!=',NULL]])
							->orderBy('grouptbl.id')
							->first();



							if($groupdata != NULL){
								$groupid = $groupdata->id;
							}else{
								$data = DB::table('grouptbl')->select('id')->where('hosttype','=',5)->get();
								foreach ($data as $key => $value) {
									$groupid =$value->id;
								}
							}


							$result4 = DB::table('leads')->where('fromemail','=',$fromemail)->update(['undergroup'=>$groupid]);

							if ($result4) {
								echo " undergroup is updated for new lead..<br>";
							}else{
								echo " failed to update undergroup for new lead<br>";
							}
							$hits1=DB::table('grouptbl')->select('hits')->where('id','=',$groupid)->get();
							foreach ($hits1 as $key => $value) {
								$newhits1=($value->hits+1);
							}
							$result5=DB::table('grouptbl')->where('id','=',$groupid)->update(['hits'=>$newhits1]);

							if ($result5) {
								echo "hits are updated for new leads...<br>";
							}else{
								echo "failed to update hits for new lead...<br>";
							}
						}
					}///end of Check blacklist and Delete

					imap_expunge($inbox);
				}
			}
		}
	}
}
