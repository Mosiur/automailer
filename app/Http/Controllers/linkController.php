<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;

class linkController extends Controller
{
	public function linkmanager(){
		$campaignid=Session::get('campaign')['campaignid'];
		$data=DB::table('linkmanager')->where('campaignid','=',$campaignid)->get();
		return view('pages.link_list',['link_list'=>$data]);
	}
	public function insertlink(Request $request){
		$link=$request->input('link');
		$token=$request->input('token');
		$campaignid=Session::get('campaign')['campaignid'];
		$affected=DB::table('linkmanager')->insert(['link'=>$link,'token'=>$token,'campaignid'=>$campaignid]);
		if($affected){
			return redirect('/linkmanager')->with('success', 'Record Store successfully!');
		}else{
			return redirect('/linkmanager')->with('failed', 'Failed to Store Rcord..!');
		}

	}
	public function edit_link(){
		$id=$_GET['id'];
		$data=DB::table('linkmanager')->where('id','=',$id)->get();
		foreach ($data as $key => $value) {
			?>
			<div class="pd-30 pd-sm-40 bg-light">
				<div class="row row-xs align-items-center mg-b-20">
					<div class="col-md-4">
						<label class="form-label mg-b-0">Link:</label>
					</div>
					<div class="col-md-8 mg-t-5 mg-md-t-0">
						<input  class="form-control" type="text" name="link" required value="<?php echo $value->link?>">
					</div>
				</div>
				<div class="row row-xs align-items-center mg-b-20">
					<div class="col-md-4">
						<label class="form-label mg-b-0">token:</label>
					</div>
					<div class="col-md-8 mg-t-5 mg-md-t-0">
						<input  class="form-control" type="text" name="token" required value="<?php echo $value->token;?>">
					</div>
				</div>
				<input type="hidden" name="id" value="<?php echo $value->id?>">
				<div class="row row-xs align-items-center mg-b-20">
					<div class="col-md-4"></div>
					<div class="col-md-8 mg-t-5 mg-md-t-0">
						<button class="btn btn-main-primary pd-x-30 mg-r-5 mg-t-5" type="submit">Save Changes</button>
						<button class="btn btn-dark pd-x-30 mg-t-5" data-dismiss="modal" type="button">Cancel</button>
					</div>
				</div>
			</div>
			<?php
		}
	}
	public function update_link(Request $request){
		$id=$request->input('id');
		$link=$request->input('link');
		$token=$request->input('token');
		$affected=DB::table('linkmanager')->Where('id','=',$id)->update(['link'=>$link,'token'=>$token]);
		if($affected){
			return redirect('/linkmanager')->with('success', 'Record Updated successfully!');
		}else{
			return redirect('/linkmanager')->with('failed', 'Failed to Update Rcord..!');
		}
	}
	public function delete_link($id){
		$affected=DB::table('linkmanager')->Where('id','=',$id)->delete();
		if($affected){
			return redirect('/linkmanager')->with('success', 'Record Deleted successfully!');
		}else{
			return redirect('/linkmanager')->with('failed', 'Failed to Delete Rcord..!');
		}

	}
}
