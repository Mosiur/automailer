<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use File;
use Image;
use Validator;
use Importer;

class smtpController extends Controller
{
	public function index(){
		$group1smtp=DB::table('smtp')->where([['smtpgroupid','=',1],['flag','=',1]])->get();
		$group2smtp=DB::table('smtp')->where([['smtpgroupid','=',2],['flag','=',1]])->get();
		$inactivesmtp1=DB::table('smtp')->where([['flag','=',0],['smtpgroupid','=',1]])->get();
		$inactivesmtp2=DB::table('smtp')->where([['flag','=',0],['smtpgroupid','=',2]])->get();
		return view('pages.smtp',['group1data'=>$group1smtp,'group2data'=>$group2smtp,'inactivedata1'=>$inactivesmtp1,'inactivedata2'=>$inactivesmtp2]);
	}
	public function inactivegroup1(Request $request){
		$data=$request->all();
		unset($data['_token']);
		unset($data['active1all']);
		// for ($i=0; $i <count($data) ; $i++) { 
		// 	$inputdata['id']=array_keys($data[$i]);
		// }
		dd($data);
		foreach ($data as $key => $row) {
			$affected=DB::table('smtp')->where('id','=',$row->id)->update(['flag'=>0]);
		}
		if($affected){
			return redirect('/smtp')->with('success', 'Record Updated successfully.!');
		}else{
			return redirect('/smtp')->with('failed', 'Record can not be updated..!');
		}
	}
	public function updateact1(Request $request){
		$id = $request->input('id');
		$flag = $request->input('flag');
		if($flag==1){
			$affected=DB::table('smtp')->where('id','=',$id)->update(['flag'=>0]);
		}
		if($flag == 0){
			$affected=DB::table('smtp')->where('id','=',$id)->update(['flag'=>1]);
		}
		
		if($affected){
			return response()->json(['success'=>'Record Updated successfully'],200);

		}else{
			return response()->json(['failed'=>'Faild to Updated Record'],200);
		}
	}

	public function delete_smtp(Request $request){

		$id = $request->input('id');
		if($id != NULL){
			$affected=DB::table('smtp')->where('id','=',$id)->delete();
		}
		
		if($affected){
			return response()->json(['success'=>'Record Deleted successfully'],200);

		}else{
			return response()->json(['failed'=>'Faild to Delete Record'],200);
		}
	}

    //Upload CSV file
	public function uploadcsv(Request $request){
		$validator = Validator::make($request->all(),
			['file' => 'required|max:5000|mimes:xls,xlsx']
		);
		if($validator->passes()){
			$time=time();
			$file=$request->file('file');
			$filename=$time.$file->getClientOriginalName();
			$savepath=public_path('assets/uploads/file/');
			$file->move($savepath,$filename);


			$excel=Importer::make('Excel');
			$excel->load($savepath.$filename);
			$collection=$excel->getCollection();
			if(sizeof($collection[1])==12){
				$success=array();
				$failed=array();
				for ($i=1; $i < sizeof($collection); $i++) { 
					try{
						$import_data[]=array(
							"campaignid" =>$collection[$i][0],
							"smtpgroupid" =>$collection[$i][1],
							"uname" =>$collection[$i][2],
							"username" =>$collection[$i][3],
							"password" =>$collection[$i][4],
							"smtphost" =>$collection[$i][4],
							"smtpport" =>$collection[$i][6],
							"smtpprotocol" =>$collection[$i][7],
							"verifiedmail" =>$collection[$i][8],
							"dailylimit" =>$collection[$i][9],
							"defaultsmtp" =>$collection[$i][10],
							"hits" =>$collection[$i][11],
							"flag"=>1
						);
						if(!empty($import_data)){
							$affected=DB::table('smtp')->insert($import_data);
							if ($affected) {
								$success[]='1';
								unset($import_data);

							}else{
								$failed[]='2';
							}
						}else{
							return redirect()->back()->with('errors','File have No Data...');
						}
						
					}catch(\Exception $e){
						return redirect()->back()
						->with(['errors'=>$e->getMessage()]);
					}
				}
				if($success!=NULL){
					return redirect()->back()->with('success','Data Uploaded Successfully');
				}
				if($failed!=NULL){

					return redirect()->back()->with('errors','Something is wrong..');
				}
			}else{
				return redirect()->back()->with(['errors'=>[0=>'Column mismatch.....!']]);
			}
		}else{
			return redirect()->back()
			->with(['errors'=>$validator->errors()->all()]);
		}
	}
}
