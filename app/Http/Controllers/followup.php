<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Session;

class followup extends Controller
{
	public function index(){
		$campaign_id=Session::get('campaign')['campaignid'];
		$campaign=$messagegroup=DB::table('campign')->select()->where('id','=',$campaign_id)->get();
		$messagegroup=DB::table('grouptbl')->where('campaignid','=',$campaign_id)->get();
		$mastermail=DB::table('mastermail')->where('campaignid','=',$campaign_id)->get();
		$followupmessage=DB::table('followupmessage')->count();
		$order=DB::table('followupmessage')->select('replynumber')->where('campaignid','=',$campaign_id)->get();
        foreach ($order as $key => $value) {
            $orderarray[]=$value->replynumber;
        }

		return view('pages.createfollowup',['campaign'=>$campaign,'messagegroup'=>$messagegroup,'mastermail'=>$mastermail,'followupmessage'=>$followupmessage,'followuporder'=>$orderarray]);

	}
	public function insert(Request $request){
		$data=$request->all();
		$data['flag']=1;
		$data['campaignid']=Session::get('campaign')['campaignid'];
		unset($data['_token']);
		$affected=DB::table('followupmessage')->insert($data);
		if($affected){
			return redirect('/all_followup')->with('success', 'Record Stored successfully!');
		}else{
			return redirect('/all_followup')->with('failed', 'Failed to store!');
		}   

	}
	public function all(){
		$campaign_id=Session::get('campaign')['campaignid'];
		$followupMessage=DB::table('followupmessage')
		->join('smtpgroup', 'smtpgroup.id', '=', 'followupmessage.smtpgroup')
		->select('followupmessage.id','followupmessage.replynumber','followupmessage.replyinterval', 'followupmessage.subject','followupmessage.messagebody','smtpgroup.groupname')
		->where('followupmessage.campaignid','=',$campaign_id)
		->get();

		return view('pages.followuplist',['followupdata'=>$followupMessage]);
	}


	public function edit($id){
            // $msgid=$_GET['id'];
		$data=DB::table('followupmessage')->select('id','replynumber','replyinterval','messagebody')->where('id','=',$id)->get();
		return view('pages.editfollowup',['followupdata'=>$data]);
	}


	public function update(Request $request,$id){
        $data=$request->all();
        unset($data['_token']);
        $result=DB::table('followupmessage')->where('id','=',$id)->update($data);
        if($result){
            return redirect('/all_followup')->with('success', 'Save Changes successfully!');
        }else{
            return redirect('/all_followup')->with('failed', 'Failed!');
        } 
        
    }


	public function delete($id){
		$delete=DB::table('followupmessage')->where('id','=',$id)->delete();
		if($delete){
			return redirect('/all_followup')->with('success', 'Record Deleted successfully!');
		}else{
			return redirect('/all_followup')->with('failed', 'Failed!');
		} 
	}
}
