<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;

class dashboard extends Controller
{
    public function index(){

    	$campaignid=Session::get('user')['campaignid'];
    	$campaignname=Session::get('campaign')['campaignname'];
    	$today=strtotime(date('Y-m-d'));

    	// Todays leads
    	$todayleads=DB::table('leads')->WHERE([['leadtime','>=',$today],['campaignid','=',$campaignid]])->count();
    	
    	//Total leads
    	$totalleads=DB::table('leads')->WHERE('campaignid','=',$campaignid)->count();
    	//Todays payout
    	$todaypayout=DB::table('postback')->where([['time','>=',$today],['campaignid','=',$campaignid]])->sum('payout');
    	//total payout
    	$totalpayout=DB::table('postback')->where('campaignid','=',$campaignid)->sum('payout');

    	//todayconv
    	$todayconv=DB::table('postback')->where([['time','>=',$today],['campaignid','=',$campaignid]])->count();
    	//totalconv
    	$totalconv=DB::table('postback')->where('campaignid','=',$campaignid)->count();

    	//today_followup
    	$today_followup=DB::table('leads')->where([['leadtime','>=',$today],['campaignid','=',$campaignid]])->sum('followup');
    	//total_followup
    	$total_followup=DB::table('leads')->where('campaignid','=',$campaignid)->sum('followup');
    	//today_reply
    	$today_reply=DB::table('leads')->where([['campaignid','=',$campaignid],['leadtime','>=',$today]])->sum('serial');
    	//total_reply
    	$total_reply=DB::table('leads')->where('campaignid','=',$campaignid)->sum('serial');
    	//latestleads
    	$latestleads=DB::table('leads')->select('fromemail','leadhosttype','mastermail','leadtime','serial','status')->where('campaignid','=',$campaignid)->orderBy('id','DESC')->limit(10)->get();

    	//total_complete
    	$total_complete=DB::table('leads')->where([['campaignid','=',$campaignid],['fstatus','=',3]])->count();
    	//today_complete
    	$today_complete=DB::table('leads')->where([['campaignid','=',$campaignid],['fstatus','=',3],['leadtime','>=',$today]])->count();



    	//todays Leads Data
    	$today_firstreply=DB::table('leads')->WHERE([['leadtime','>=',$today],['campaignid','=',$campaignid],['status','=',1],['serial','>=',1]])->count();
    	$today_secondreply=DB::table('leads')->WHERE([['leadtime','>=',$today],['campaignid','=',$campaignid],['status','=',1],['serial','>=',2]])->count();
    	$today_3rdreply=DB::table('leads')->WHERE([['leadtime','>=',$today],['campaignid','=',$campaignid],['status','=',1],['serial','>=',3]])->count();
    	$today_4threply=DB::table('leads')->WHERE([['leadtime','>=',$today],['campaignid','=',$campaignid],['status','=',1],['serial','>=',4]])->count();
    	$today_5threply=DB::table('leads')->WHERE([['leadtime','>=',$today],['campaignid','=',$campaignid],['status','=',1],['serial','>=',5]])->count();
    	$today_6threply=DB::table('leads')->WHERE([['leadtime','>=',$today],['campaignid','=',$campaignid],['status','=',1],['serial','>=',6]])->count();
    	$today_7threply=DB::table('leads')->WHERE([['leadtime','>=',$today],['campaignid','=',$campaignid],['status','=',1],['serial','>=',7]])->count();
    	$today_8threply=DB::table('leads')->WHERE([['leadtime','>=',$today],['campaignid','=',$campaignid],['status','=',1],['serial','>=',8]])->count();
    	$today_9threply=DB::table('leads')->WHERE([['leadtime','>=',$today],['campaignid','=',$campaignid],['status','=',1],['serial','>=',9]])->count();
    	$today_10threply=DB::table('leads')->WHERE([['leadtime','>=',$today],['campaignid','=',$campaignid],['status','=',1],['serial','>=',10]])->count();
    	$today_1stwaiting=DB::table('leads')->WHERE([['leadtime','>=',$today],['campaignid','=',$campaignid],['status','=',0],['serial','>=',1]])->count();




    	//total Leads Data
    	$total_firstreply=DB::table('leads')->WHERE([['campaignid','=',$campaignid],['status','=',1],['serial','>=',1]])->count();
    	$total_secondreply=DB::table('leads')->WHERE([['campaignid','=',$campaignid],['status','=',1],['serial','>=',2]])->count();
    	$total_3rdreply=DB::table('leads')->WHERE([['campaignid','=',$campaignid],['status','=',1],['serial','>=',3]])->count();
    	$total_4threply=DB::table('leads')->WHERE([['campaignid','=',$campaignid],['status','=',1],['serial','>=',4]])->count();

    	$total_5threply=DB::table('leads')->WHERE([['campaignid','=',$campaignid],['status','=',1],['serial','>=',5]])->count();
    	$total_6threply=DB::table('leads')->WHERE([['campaignid','=',$campaignid],['status','=',1],['serial','>=',6]])->count();
    	$total_7threply=DB::table('leads')->WHERE([['campaignid','=',$campaignid],['status','=',1],['serial','>=',7]])->count();
    	$total_8threply=DB::table('leads')->WHERE([['campaignid','=',$campaignid],['status','=',1],['serial','>=',8]])->count();
    	$total_9threply=DB::table('leads')->WHERE([['campaignid','=',$campaignid],['status','=',1],['serial','>=',9]])->count();
    	$total_10threply=DB::table('leads')->WHERE([['campaignid','=',$campaignid],['status','=',1],['serial','>=',10]])->count();
    	$total_1stwaiting=DB::table('leads')->WHERE([['campaignid','=',$campaignid],['status','=',0],['serial','>=',1]])->count();


    	//monthlyleads
    	$prevmonth = date("Y-m-d", strtotime("last Month"));

    	$monthlyleads=DB::table('leads')->where([['campaignid','=',$campaignid],['status','=',1],['leadtime','>=',$prevmonth],['leadtime','<=',$today]])->count();

    	$total_waiting=DB::table('leads')->where([['campaignid','=',$campaignid],['status','=',0]])->count();

    	$today_waiting=DB::table('leads')->where([['campaignid','=',$campaignid],['status','=',0],['leadtime','>=',$today],['leadtime','<=',$today]])->count();

    	// others
    	$campaign=DB::table('campign')->where('id','=',$campaignid)->get();


    	$users=DB::table('users')
			->join('campign','campign.id','=','users.campaignid')
			->select('users.*','campign.campaignname' ,'campign.campaignlimit')
			->orderBy('users.id','DESC')
            ->where('users.campaignid','=',$campaignid)
			->get();


    	$smtpgroup=DB::table('smtpgroup')
			->join('campign','campign.id','=','smtpgroup.campaignid')
			->select('smtpgroup.*','campign.campaignname' ,'campign.campaignlimit')
            ->where('smtpgroup.campaignid','=',$campaignid)
			->orderBy('smtpgroup.id','DESC')
			->get();

    	return view('pages.dashboard',[
    		'todayleads'=>$todayleads,
    		'totalleads'=>$totalleads,
    		'todaypayout'=>$todaypayout,
    		'totalpayout'=>$totalpayout,
    		'todayconv'=>$todayconv,
    		'totalconv'=>$totalconv,
    		'today_followup'=>$today_followup,
    		'total_followup'=>$total_followup,
    		'today_reply'=>$today_reply,
    		'total_reply'=>$total_reply,
    		'latestleads'=>$latestleads,
    		'total_complete'=>$total_complete,
    		'today_complete'=>$today_complete,
    		'today_firstreply'=>$today_firstreply,
    		'today_secondreply'=>$today_secondreply,
    		'today_3rdreply'=>$today_3rdreply,
    		'today_4threply'=>$today_4threply,
    		'today_5threply'=>$today_5threply,
    		'today_6threply'=>$today_6threply,
    		'today_7threply'=>$today_7threply,
    		'today_8threply'=>$today_8threply,
    		'today_9threply'=>$today_9threply,
    		'today_10threply'=>$today_10threply,
    		'today_1stwaiting'=>$today_1stwaiting,
    		'total_firstreply'=>$total_firstreply,
    		'total_secondreply'=>$total_secondreply,
    		'total_3rdreply'=>$total_3rdreply,
    		'total_4threply'=>$total_4threply,
    		'total_5threply'=>$total_5threply,
    		'total_6threply'=>$total_6threply,
    		'total_7threply'=>$total_7threply,
    		'total_8threply'=>$total_8threply,
    		'total_9threply'=>$total_9threply,
    		'total_10threply'=>$total_10threply,
    		'total_1stwaiting'=>$total_1stwaiting,
    		'monthlyleads'=>$monthlyleads,
    		'total_waiting'=>$total_waiting,
    		'today_waiting'=>$today_waiting,
    		'campaign'=>$campaign,
    		'users'=>$users,
    		'smtpgroup'=>$smtpgroup
    	]);
    }
}
