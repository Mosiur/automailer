<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;

class resetController extends Controller
{
	public function reset_system(){
		$tables = DB::select('SHOW TABLES');
		return view('pages.resetsystem',['table'=>$tables]);
	}
	public function resetsystem(Request $request){
		$campaignid=Session::get('campaign')['campaignid'];
		$data=$request->all();
		unset($data['_token']);
		$tablenm=array();

		
		$tables = DB::select('SHOW TABLES');
		foreach ($tables as $key => $value) {
			if(array_key_exists($value->Tables_in_mailresponder, $data)){
				$tablenm[]=$value->Tables_in_mailresponder;
			}
		}
		$success=array();
		$failed=array();
		for ($i=0; $i <count($tablenm) ; $i++) {
			if($tablenm[$i] == 'campign'){
				DB::table($tablenm[$i])->where('id','=',$campaignid)->delete();
				$success[]=$tablenm[$i];
			}
			else if(DB::table($tablenm[$i])->where('campaignid','=',$campaignid)->delete()){
				$success[]=$tablenm[$i];
			}else{
				$failed[]=$tablenm[$i];
			}
		}

		if($success!=NULL){
			return redirect('/reset_system')->with('success', 'System Reset Successfully.....');
		}
		if($failed!=NULL){
			return redirect('/reset_system')->with('failed', 'Already Reset...');
		}
	}
}
