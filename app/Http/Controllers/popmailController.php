<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;

class popmailController extends Controller
{
    public function addpopmail(){
        $campaignid=Session::get('campaign')['campaignid'];
        $campaign= DB::table('campign')->select('id','campaignname')->where('id','=',$campaignid)->get();
            ?>
            <!-- Row -->
            <div class="card-body">
                <div class="main-content-label mg-b-5">
                    Add Master Mail or POP mail
                </div>
                <div class="pd-30 pd-sm-40 bg-light">
                    <div class="row row-xs align-items-center mg-b-20">
                        <div class="col-md-4">
                            <label class="form-label mg-b-0">Campaign</label>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            <select class="form-control select2-no-search" name="campaignid">
                                <?php
                                foreach ($campaign as $key => $camvalue) {?>
                                    <option value="<?php echo $camvalue->id;?>">
                                        <?php echo $camvalue->campaignname;?>
                                    </option>
                                    <?php 
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-20">
                        <div class="col-md-4">
                            <label class="form-label mg-b-0">Email Id</label>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            <input name="emailid" class="form-control" placeholder="Enter Email ID" type="text">
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-20">
                        <div class="col-md-4">
                            <label class="form-label mg-b-0">Email Password</label>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            <input name="emailpass" class="form-control" placeholder="Enter your email Password" type="text">
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-20">
                        <div class="col-md-4">
                            <label class="form-label mg-b-0">Host Name</label>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            <input  name="emailhost" class="form-control" placeholder="Enter email Host Name" type="text">
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-20">
                        <div class="col-md-4">
                            <label class="form-label mg-b-0">Port</label>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            <input  name="emailport" class="form-control" placeholder=" for gmail port=993" type="text">
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-20">
                        <div class="col-md-4">
                            <label class="form-label mg-b-0">Protocol</label>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            <select class="form-control select2-no-search" name="protocol">
                                <option value="1">SSL</option>
                                <option value="2">TLS</option>
                            </select>
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-20">
                        <div class="col-md-4">
                            <label class="form-label mg-b-0">Recovery Mail (Optional)</label>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            <input  name="recoverymail" class="form-control" placeholder="Enter your recovery email" type="email">
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-20">
                        <div class="col-md-4">
                            <label class="form-label mg-b-0">Status</label>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            <select class="form-control select2-no-search" name="flag">
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                            </select>
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-20">
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            <button class="btn btn-main-primary pd-x-30 mg-r-5 mg-t-5" type="submit">Save Changes</button>
                            <button class="btn btn-dark pd-x-30 mg-t-5" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
            <!--/Row-->
        <?php
    }
    public function insertpopmail(Request $request){
        $data=$request->all();
        unset($data['_token']);
        $data['dateotime']=time();
        $affected=DB::table('mastermail')->insert($data);
        if($affected){
            return redirect('/popmail')->with('success', 'Record Added successfully!');
        }else{
            return redirect('/popmail')->with('failed', 'Failed!');
        }


    }
    public function index(){
        $campaignid=Session::get('campaign')['campaignid'];
    	$campaign= DB::table('campign')->select('id','campaignname')->where('id','=',$campaignid)->get();
    	$mastermails = DB::table('mastermail')->where('campaignid','=',$campaignid)->get();
    	return view('pages/popmail', ['mastermails' => $mastermails,'campaigns'=>$campaign]);
    }
    public function edit_mastermail(){
        $id=$_GET['id'];
        $campaignid=Session::get('campaign')['campaignid'];
        $campaign= DB::table('campign')->select('id','campaignname')->where('id','=',$campaignid)->get();
        $data=DB::table('mastermail')->WHERE('id','=',$id)->get();
        foreach ($data as $key => $value) {
            ?>
            <!-- Row -->
            <div class="card-body">
                <div class="main-content-label mg-b-5">
                    Edit Master Mail
                </div>
                <div class="pd-30 pd-sm-40 bg-light">
                    <div class="row row-xs align-items-center mg-b-20">
                        <div class="col-md-4">
                            <label class="form-label mg-b-0">Campaign</label>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            <select class="form-control select2-no-search" name="campign">
                                <?php
                                foreach ($campaign as $key => $camvalue) {?>
                                    <option value="<?php echo $camvalue->id;?>" <?php if($camvalue->id == $value->campaignid){ echo "selected";}?>><?php echo $camvalue->campaignname;?></option>
                                <?php  }?>
                            </select>
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-20">
                        <div class="col-md-4">
                            <label class="form-label mg-b-0">Email Id</label>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            <input name="emailid" class="form-control" placeholder="Enter Email ID" type="text" value="<?php echo $value->emailid;?>">
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-20">
                        <div class="col-md-4">
                            <label class="form-label mg-b-0">Email Password</label>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            <input name="emailpass" class="form-control" placeholder="Enter your email Password" type="text" value="<?php echo $value->emailpass;?>">
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-20">
                        <div class="col-md-4">
                            <label class="form-label mg-b-0">Host Name</label>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            <input  name="emailhost" class="form-control" placeholder="Enter email Host Name" type="text" value="<?php echo $value->emailhost;?>">
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-20">
                        <div class="col-md-4">
                            <label class="form-label mg-b-0">Port</label>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            <input  name="emailport" class="form-control" placeholder="Enter email port" type="text" value="<?php echo $value->emailport;?>">
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-20">
                        <div class="col-md-4">
                            <label class="form-label mg-b-0">Protocal</label>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            <select class="form-control select2-no-search" name="protocal">
                                <option value="1" <?php if($value->protocol == 1){
                                    echo "selected";}?>>SSL</option>
                                <option value="2" <?php if($value->protocol == 2){
                                    echo "selected";}?>>TLS</option>
                            </select>
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-20">
                        <div class="col-md-4">
                            <label class="form-label mg-b-0">Recovery Mail (Optional)</label>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            <input  name="recoverymail" class="form-control" placeholder="Enter your recovery email" type="email" value="<?php echo $value->recoverymail;?>">
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-20">
                        <div class="col-md-4">
                            <label class="form-label mg-b-0">Status</label>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            <select class="form-control select2-no-search" name="flag">
                                <option value="1" <?php if($value->flag == 1){
                                    echo "selected";}?>>Active</option>
                                <option value="0" <?php if($value->flag == 0){
                                    echo "selected";}?>>Inactive</option>
                            </select>
                        </div>
                    </div>
                    <input type="hidden" name="id" class="form-control" type="text" value="<?php echo $value->id;?>">
                    <button class="btn btn-main-primary pd-x-30 mg-r-5 mg-t-5" type="submit">Save Changes</button>
                    <button class="btn btn-dark pd-x-30 mg-t-5" data-dismiss="modal">Cancel</button>
                </div>
            </div>
            <!--/Row-->
            <?php
        }

    }
    public function update(Request $request){
        $id = $request->input('id');
        $campign = $request->input('campign');
        $emailid = $request->input('emailid');
        $emailpass = $request->input('emailpass');
        $emailhost = $request->input('emailhost');
        $emailport=$request->input('emailport');
        $protocal = $request->input('protocal');
        $recoverymail = $request->input('recoverymail');
        $flag= $request->input('flag');
        $data = ['campaignid' => $campign,'emailid' => $emailid ,'emailpass' => $emailpass,'emailhost' => $emailhost,'emailport' => $emailport,'recoverymail' => $recoverymail,'protocol'=>$protocal,'flag'=>$flag];
        $affected=DB::table('mastermail')->where('id',$id)->update($data);
        if($affected){
            return redirect('/popmail')->with('success', 'Record Updated successfully!');
        }else{
            return redirect('/popmail')->with('failed', 'Record Updated Failed!');
        }
    }
}
