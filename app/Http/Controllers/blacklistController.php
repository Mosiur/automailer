<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;

class blacklistController extends Controller
{
	public function blacklist(){
		$campaignid=Session::get('campaign')['campaignid'];
		$data=DB::table('blacklist')->where('campaignid','=',$campaignid)->orderBy('id', 'DESC')->get();
		return view('pages.blacklist',['listdata'=>$data]);

	}
	public function add(Request $request){
		$matchword=$request->input('matchword');
		$campaignid=Session::get('campaign')['campaignid'];
		$result=DB::table('blacklist')->where([['matchword','=',$matchword],['campaignid','=',$campaignid]])->count();
		if($result<1){
			$flag=1;
			$affected=DB::table('blacklist')->insert(['matchword'=>$matchword,'flag'=>$flag,'campaignid'=>$campaignid]);
			if($affected){
				return redirect('/blacklist')->with('success', 'Record Stored successfully!');
			}else{
				return redirect('/blacklist')->with('failed', 'Failed to store!');
			} 
		}else{
			return redirect('/blacklist')->with('failed', 'Keyword Already Blacklisted...!');
		}
	}
	public function edit(){
		$id=$_GET['id'];
		$result=DB::table('blacklist')->where('id','=',$id)->get();
		foreach ($result as $key => $value) {
			?>
			<div class="form-group">
				<label for="message-text" class="col-form-label">Keyword:</label>
				<textarea class="form-control" name="matchword" placeholder="example:noreplay"><?php echo $value->matchword;?></textarea>
				<input type="hidden" name="id" value="<?php echo $value->id;?>">
			</div>
			<?php
		}

	}
	public function update(Request $request){
		$id=$request->input('id');
		$matchword=$request->input('matchword');
		$flag=1;
		$affected=DB::table('blacklist')->where('id','=',$id)->update(['matchword'=>$matchword,'flag'=>$flag]);
		if($affected){
			return redirect('/blacklist')->with('success', 'Record Changed successfully!');
		}else{
			return redirect('/blacklist')->with('failed', 'Failed to change!');
		}

	}
	public function delete($id){
		$affected=DB::table('blacklist')->where('id','=',$id)->delete();
		if($affected){
			return redirect('/blacklist')->with('success', 'Record deleted successfully!');
		}else{
			return redirect('/blacklist')->with('failed', 'Failed to delete..!');
		}

	}
}
