<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
class replayMessage extends Controller
{
    public function index(){
        $campaign_id=Session::get('campaign')['campaignid'];
        $campaign=$messagegroup=DB::table('campign')->select()->where('id','=',$campaign_id)->get();
        $messagegroup=DB::table('grouptbl')->where('campaignid','=',$campaign_id)->get();
        $mastermail=DB::table('mastermail')->where('campaignid','=',$campaign_id)->get();
        $replayMessage=DB::table('replymessage')->where('campaignid','=',$campaign_id)->count();
        $order=DB::table('replymessage')->select('replynumber')->where('campaignid','=',$campaign_id)->get();
        foreach ($order as $key => $value) {
            $orderarray[]=$value->replynumber;
        }

        return view('pages.create_replay',['campaign'=>$campaign,'messagegroup'=>$messagegroup,'mastermail'=>$mastermail,'replayMessage'=>$replayMessage,'array'=>$orderarray]);
    }

    public function insert(Request $request){

        $data=$request->all();
        $data['flag']=1;
        $data['campaignid']=Session::get('campaign')['campaignid'];
        unset($data['_token']);
        $affected=DB::table('replymessage')->insert($data);
        if($affected){
            return redirect('/all_replay')->with('success', 'Record Stored successfully!');
        }else{
            return redirect('/all_replay')->with('failed', 'Failed to store!');
        }   

    }


    public function allreplay(){
        $campaign_id=Session::get('campaign')['campaignid'];
        $replayMessage=DB::table('replymessage')
        ->join('smtpgroup', 'smtpgroup.id', '=', 'replymessage.smtpgroup')
        ->select('replymessage.id','replymessage.replynumber','replymessage.replyinterval', 'replymessage.subject','replymessage.messagebody','smtpgroup.groupname')
        ->where('replymessage.campaignid','=',$campaign_id)
        ->get();

        return view('pages.all_replay',['msgdata'=>$replayMessage]);
    }

    public function edit_by_modal(){
        $msgid=$_GET['id'];
        $data=DB::table('replymessage')->select('replynumber','replyinterval','messagebody')->where('id','=',$msgid)->get();
        foreach ($data as $value) {?>
            <div class="row row-xs align-items-center mg-b-20">
                <div class="col-md-3">
                    <label class="form-label mg-b-0">Order By:</label>
                </div>
                <div class="col-md-9 mg-t-5 mg-md-t-0">
                    <select class="form-control select2-no-search" name="replynumber" required="required">
                        <option <?php if($value->replynumber == 1){ echo "selected";}?> value="1">1</option>
                        <option <?php if($value->replynumber == 2){ echo "selected";}?>value="2">2</option>
                        <option <?php if($value->replynumber == 3){ echo "selected";}?>value="3">3</option>
                        <option <?php if($value->replynumber == 4){ echo "selected";}?>value="4">4</option>
                        <option <?php if($value->replynumber == 5){ echo "selected";}?>value="5">5</option>
                        <option <?php if($value->replynumber == 6){ echo "selected";}?>value="6">6</option>
                    </select>
                </div>
            </div>

            <div class="row row-xs align-items-center mg-b-20">
                <div class="col-md-3">
                    <label class="form-label mg-b-0">Delay time (min):</label>
                </div>
                <div class="col-md-9 mg-t-5 mg-md-t-0">
                    <input name="replyinterval" class="form-control" placeholder="Enter delay time for send message" type="Number" value="<?php echo $value->replyinterval;?>" required="required" min="1">
                </div>
            </div>
            <div class="row row-xs align-items-center mg-b-20">
                <div class="col-md-3">
                    <label class="form-label mg-b-0">Messsage:</label>
                </div>
                <div class="col-md-9 mg-t-5 mg-md-t-0">
                    <textarea class="editor1" name="messagebody"><?php echo strip_tags(htmlspecialchars_decode($value->messagebody))?></textarea>
                </div>
            </div>
            <?php
        }
    }
    public function edit($id){
            // $msgid=$_GET['id'];
        $data=DB::table('replymessage')->select('id','replynumber','replyinterval','messagebody')->where('id','=',$id)->get();
        return view('pages.editmsg',['msgdata'=>$data]);
    }
    public function update(Request $request,$id){
        $data=$request->all();
        unset($data['_token']);
        $result=DB::table('replymessage')->where('id','=',$id)->update($data);
        if($result){
            return redirect('/all_replay')->with('success', 'Save Changes successfully!');
        }else{
            return redirect('/all_replay')->with('failed', 'Failed!');
        } 
        
    }
    public function delete($id){
        $delete=DB::table('replymessage')->where('id','=',$id)->delete();
        if($delete){
            return redirect('/all_replay')->with('success', 'Record Deleted successfully!');
        }else{
            return redirect('/all_replay')->with('failed', 'Failed!');
        } 
    }
}
