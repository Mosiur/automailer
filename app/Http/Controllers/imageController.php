<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Session;
use File;
use Image;

// install "composer require intervention/image" via composer than you can use Image class and Add provider to provider class ->Intervention\Image\ImageServiceProvider::class, and add alises to alise class->'Image' => Intervention\Image\Facades\Image::class, more details got to http://image.intervention.io/getting_started/installation


class imageController extends Controller
{
    public function index(){
        $campaignid=Session::get('campaign')['campaignid'];
        $data=DB::table('imagemanager')->where('campaignid','=',$campaignid)->get();
        return view('pages.imagemanager',['image_list'=>$data]);
    }
    public function insert(Request $request){
        $campaignid=Session::get('campaign')['campaignid'];
        $name=$request->input('name');
    	$image=$request->file('img');//File Name
    	$extention=$image->getClientOriginalExtension();//File Extention
    	$newimage_name=time().'.'.$extention;
    	$image_resize = Image::make($image->getRealPath());   
    	$image_resize->resize(255,291);
    	$image_resize->save('assets/uploads/'.$newimage_name);
    	$path="assets/uploads/".$newimage_name;

    	$affected=DB::table('imagemanager')->insert(['campaignid'=>$campaignid,'imagelink'=>$path,'name'=>$name]);
      if($affected){
       return redirect('/imagemanager')->with('success', 'Record Store successfully!');
   }else{
       return redirect('/imagemanager')->with('failed', 'Failed to Store Rcord..!');
   }
}
public function edit_image(){
    $id=$_GET['id'];
    $data=DB::table('imagemanager')->WHERE('id','=',$id)->get();
    foreach ($data as $key => $value) {
        ?>
        <div class="modal-header">
            <h6 class="modal-title">Edit Image</h6>
        </div>
        <div class="modal-body" id="linkform">
            <div class="pd-30 pd-sm-40 bg-light">
                <div class="row row-xs align-items-center mg-b-20">
                    <div class="col-md-4">
                        <label class="form-label mg-b-0">Name:</label>
                    </div>
                    <div class="col-md-8 mg-t-5 mg-md-t-0">
                        <input  class="form-control" type="text" name="name" required placeholder="write a name of an image which is use to identify" value="<?php echo $value->name;?>">
                    </div>
                </div>
                <div class="row row-xs align-items-center mg-b-20">
                    <div class="col-md-4">
                        <label class="form-label mg-b-0">Image:</label>
                    </div>
                    <div class="col-md-8 mg-t-5 mg-md-t-0">
                        <input class="form-control" type="file" name="img">
                    </div>
                </div>
                    <input type="hidden" name="id" class="form-control" type="text" value="<?php echo $value->id;?>">
                
                <div class="row row-xs align-items-center mg-b-20">
                    <div class="col-md-4"></div>
                    <div class="col-md-8 mg-t-5 mg-md-t-0">
                        <button class="btn btn-main-primary pd-x-30 mg-r-5 mg-t-5" type="submit">Save</button>
                        <button class="btn btn-dark pd-x-30 mg-t-5" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </div>
            </div>

        </div>
        <?php
    }
}

public function updt_image(Request $request){
    $id= $request->input('id');
    $name=$request->input('name');
    $image=$request->file('img');//File Name
    
    if($request->hasfile('img')){
        // delete previous image from directory
        $imglink=DB::table('imagemanager')->select('imagelink')->Where('id','=',$id)->first();
        unlink($imglink->imagelink);

        $image=$request->file('img');//File Name       
        $extention=$image->getClientOriginalExtension();//File Extention
        $newimage_name=time().'.'.$extention;
        $image_resize = Image::make($image->getRealPath());   
        $image_resize->resize(255,291);
        $image_resize->save('assets/uploads/'.$newimage_name);
        $path="assets/uploads/".$newimage_name;

        $affected=DB::table('imagemanager')->WHERE('id',$id)->update(['imagelink'=>$path,'name'=>$name]);
        
        if($affected){
            return redirect('/imagemanager')->with('success', 'Record Updated successfully!');
        }else{
            return redirect('/imagemanager')->with('failed', 'Record Updated Failed!');
        }
    }else{
        $affected=DB::table('imagemanager')->WHERE('id',$id)->update(['name'=>$name]);
        
        if($affected){
            return redirect('/imagemanager')->with('success', 'Record Updated successfully!');
        }else{
            return redirect('/imagemanager')->with('failed', 'Record Updated Failed!');
        }
    }
}
public function delete($id){
 $imglink=DB::table('imagemanager')->select('imagelink')->Where('id','=',$id)->first();
 $affected=DB::table('imagemanager')->Where('id','=',$id)->delete();
 if($affected){
   unlink($imglink->imagelink);
   return redirect('/imagemanager')->with('success', 'Record Deleted successfully!');
}else{
   return redirect('/imagemanager')->with('failed', 'Failed to Delete Rcord..!');
}
}
}
