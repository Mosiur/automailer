<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
// for install PHPMailer run command =composer require phpmailer/phpmailer
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use DB;
use Session;

class PhpmailerController extends Controller{

	public function sendEmail () {
			require '../vendor/autoload.php';	// load Composer's autoloader

			//$mail = new PHPMailer(true);  // Passing `true` enables exceptions

			$allleads=DB::table('leads')
			->where([['status','=',0],['serial','=',1]])
			->orderBY('id')
			->limit(10)
			->get();

			if ($allleads) {
            	//dd($allleads);
				foreach ($allleads as $leadsdata) {
					$leadid             = $leadsdata->id;
					$campaignid         = $leadsdata->campaignid;
					$mastermail         = $leadsdata->mastermail;
					$fromname           = $leadsdata->fromname;
					$fromemail          = $leadsdata->fromemail;
					$leadhosttype       = $leadsdata->leadhosttype;
					$msgbody            = $leadsdata->msgbody;
					$udate              = $leadsdata->udate;
					$leadsubject        = $leadsdata->subject;
					$serial             = $leadsdata->serial;
					$undergroup         = $leadsdata->undergroup;
					$message_id         = $leadsdata->message_id;
					$leadtime           = $leadsdata->leadtime;

                	///Leads
					echo     $lead = "$fromemail@$leadhosttype";
					$altbdy = $leadhosttype;



					$replyset=DB::table('replymessage')
					->join('smtpgroup','smtpgroup.id','=','replymessage.smtpgroup')
					->join('smtp','smtp.smtpgroupid','=','smtpgroup.id')
					->select('replymessage.*', 'smtp.id as smtpid' , 'smtp.uname as uname' ,'smtp.username as smtpusername', 'smtp.password as smtppassword', 'smtp.smtphost', 'smtp.smtpport', 'smtp.smtpprotocol', 'smtp.verifiedmail as smtpverifiedmail','smtp.dailylimit as smtpdailylimit' , 'smtp.hits as  smtphits','smtpgroup.id as smtpgroupid')
					->where([
						['replymessage.messagegroup','=',1],
						['replymessage.replynumber','=',$serial]])
					->whereColumn('smtp.hits','<','smtp.dailylimit')
					->limit(1)
					->get();

					//dd($replyset);

					if ($replyset == NULL) {

						$replymessage_defaul=DB::table('replymessage')
						->join('smtpgroup','smtpgroup.id','=','replymessage.smtpgroup')
						->join('smtp','smtp.smtpgroupid','=','smtpgroup.id')
						->select('replymessage.*','smtp.*', 'smtpgroup.*')
						->where([
							['replymessage.messagegroup','=',1],
							['replymessage.replynumber','=',$serial],
							['smtpgroup.defaultsmtp','=',1]])
						->orderBY('replymessage.id')
						->limit(1)
						->get();

						// dd($replymessage_defaul);

					} else {

						foreach ($replyset as $row) {
							$messsageid      = $row->id;
							$replyinterval   = $row->replyinterval;
							$sendername      = $row->sendername;
							$senderemail     = $row->smtpusername;
							$replyto         = $row->replyto;
							$subject         = $row->subject;
							$messagebody     = $row->messagebody;
							$uname           = $row->uname;
							$username        = $row->smtpusername;
							$password        = $row->smtppassword;
							$smtphost        = $row->smtphost;
							$smtpport        = $row->smtpport;
							$smtpprotocol    = $row->smtpprotocol;
							$smtpid          = $row->smtpid;


							if ($smtpprotocol == 1) {
								$smtpprotocol = "SSL";
							} elseif (2) {
								$smtpprotocol = "TLS";
							} else {
								$smtpprotocol = "No";
							}

                        	///Create subject with token///

							if (strpos(strtolower($subject), "{subject}")) {
								$subject = str_replace("{subject}", $leadsubject, $subject);
							}

                        	///Token action//	
							$todayis    = date('l');
							$todate     = date("M-d-Y");
							if(is_array($fromname)){
								$namearray  = explode(" ", $fromname);
								$firstname = $namearray[0];
								$lastname  = $namearray[1];
							}else{
								$firstname = $fromname;
								$lastname  = ' ';
							}
							

							if (strpos($messagebody, "{fname}")) {
								$messagebody =  str_replace("{fname}", $firstname, $messagebody);
							}

							if (strpos($messagebody, "{lname}")) {
								$messagebody =  str_replace("{lname}", $lastname, $messagebody);
							}

							if (strpos($messagebody, "{fullname}")) {
								$messagebody =  str_replace("{fullname}", $fullname, $messagebody);
							}

							if (strpos($messagebody, "{email}")) {
								$messagebody =  str_replace("{email}", $yourmail, $messagebody);
							}

							if (strpos($messagebody, "{todayis}")) {
								$messagebody =  str_replace("{todayis}", $todayis, $messagebody);
							}

							if (strpos($messagebody, "{date}")) {
								$messagebody =  str_replace("{date}", $todate, $messagebody);
							}


							$body = $messagebody;
							$text = $body;

							$text = preg_replace_callback('/\{([^}]+)\}/', function ($match) {
								return $a[array_rand($a = explode('|', $match[1]))];
							}, $text);



							$bodyx   = trim($text);

							$altbody = strip_tags($bodyx);


                        	//echo "SMTP limit: ". $smtpdailylimit		= $row->smtpdailylimit;
							
							$campaignlimitbyiddata=DB::table('campign')
							->select('campaignlimit')
							->where('id','=',$campaignid)
							->get();

							foreach ($campaignlimitbyiddata as $key => $row) {
								$campaignlimitbyid = $row->campaignlimit;
							}

							$todaycmapingsent = DB::table('leads')->select('id')->where([['leadtime','=',time()],['campaignid','=',$campaignid],['status','=',1]])->count();

							if ($campaignlimitbyid > $todaycmapingsent) {
								$intervalinsec = time() - $leadtime;
								echo " Minute different : " . $intervalinmin = $intervalinsec  / 60;
								if ($replyinterval <  $intervalinmin) {

                                	//Relay
									$smtpx=DB::table('smtp')
									->where([
										['smtpgroupid','=',2],
										['verifiedmail','=',1]
									])
									->get();

									if ($smtpx) {
										foreach ($smtpx as $smtpdata) {
											$smtpx = $smtpdata->username;

											$mail = "";
											$this->mail = new PHPMailer();
											$this->mail->SMTPOptions = array(
												'ssl' => array(
													'verify_peer' => false,
													'verify_peer_name' => false,
													'allow_self_signed' => true
												)
											);

											$this->mail->SMTPDebug = 0;
											$this->mail->isHTML(true);
											$this->mail->isSMTP();
											$this->mail->Host = $smtphost;
											$this->mail->Port = $smtpport;
											$this->mail->SMTPSecure = $smtpprotocol;
											$this->mail->SMTPAuth = true;


											$this->mail->Username = $username;
											$this->mail->Password = $password;
											$this->mail->From =  $uname;
											$this->mail->FromName = $sendername;
											$this->mail->AddReplyTo($smtpx);
											$this->mail->addCustomHeader('In-Reply-To', $message_id);


											$this->mail->addAddress($lead);
											$this->mail->Subject = $subject;
											$this->mail->AltBody  =  $altbody;
											$this->mail->Body = $bodyx;
											$this->mail->Body .= $msgbody;
											$this->mail->Body .= "<img border='0'
											src='{{route('etrack', ['email'=>$fromemail,'id'=>$leadtime])}}' width='0' height='0'  ></body></html>";


											$attachment = DB::table('attachment')
											->select('filename')
											->where([['messageid','=',$messsageid],['messagetype','=',1]])
											->get();
											if ($attachment) {
												foreach ($attachment as $attfile) {
													$filename = $attfile->filename;
													$filepath = "attachment/" . $filename;
													$this->mail->addAttachment($filepath, $filename);
												}
											} else {
												echo "Attachment Not Found";
											}

											if ($this->mail->Send()) {
												echo " MAIL SENT ";
                                            	///Lead Status
												$affec1=DB::table('leads')->where('id','=',$leadid)->update(['status'=>1]);
												if($affec1){
													echo "lead Status updated...successfully";
												}else{
													echo "failed to update lead status";
												}

												$hits2=DB::table('smtp')->select('hits')->where('id','=',$smtpid)->get();

												foreach ($hits2 as $key => $row) {
													$newhits2=($row->hits+1);
												}
												$result1=DB::table('smtp')->where('id','=',$smtpid)->update(['hits'=>$newhits2]);
												if ($result1) {
													echo " hits are updated for group table...<br>";
												}else{
													echo "hits can not be updated for group table...<br>";
												}

												DB::table('leads')->where('id','=',$leadid)->update(['freply'=>$smtpx]);
												DB::table('smtp')->where('username','=',$smtpx)->update(['verifiedmail'=>0 ]);

                                            	///SMTP HITS

											} else {
												DB::table('smtp')->where('id','=',$smtpid)->update(['smtpgroupid'=>3,'flag'=>0]);
											}
										}
									} else {
										echo "You have no articles added!";
										DB::table('smtp')->where('smtpgroupid','=',2)->update(['verifiedmail'=>1]);
									}

                                //relay end
								} else {
									echo " Time Different is Lower<br> ";
								}
							}
						}
					}
            } /// foreach leads//
        } else {
        	echo "= No leads found = ";
        } //if leads is not empty
    }
    public function etrack($email,$id){
    	if (!empty($_GET['email'])) {
    		$id = $_GET['id'];
    		$checkid = "Id:" . $id;
    		$email = $_GET['email'];
    		$sub = $_GET['subject'];
    		date_default_timezone_set('Asia/Kolkata');
    		$date = date('d/m/Y h:i:s a');
    		DB::table('leads')->where([['fromemail','=',$email],['leadtime','=',$id]])->update(['eopen'=>'eopen'+1,'leadtime'=>'leadtime'+1]);
    		//All done, get out!
    		exit;
    	}	
    }
} 
