<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

//cache clear
Route::get('/clear-cache', function() {
	Artisan::call('cache:clear');
	return "Cache is cleared";
});
Route::get('/admin', function () { return view('pages/login');});
Route::post('/signin','auth@index')->name('signin');
Route::group(['middleware'=>['userAuthentication']],function(){
	
	Route::get('/','dashboard@index');
	Route::get('/autoload', function () {return view('layouts.autoload');});


	Route::get('/managecampaign','campaignController@index');
	Route::get('/edit_campaign','campaignController@edit')->name('edit_campaign');
	Route::post('/updt_campaign','campaignController@update');


	Route::get('/addpopmail','popmailController@addpopmail');
	Route::post('/insertpopmail','popmailController@insertpopmail');
	Route::get('/popmail','popmailController@index');
	Route::get('/edit_mastermail','popmailController@edit_mastermail');
	Route::post('/updt_mastermail','popmailController@update');


	//image manager
	Route::get('/imagemanager','imageController@index');
	Route::post('/insertimage','imageController@insert');
	Route::get('/edit_image','imageController@edit_image');
	Route::post('/updt_image','imageController@updt_image');
	Route::get('/delete_image/{id}','imageController@delete')->name('delete_image');

	// Replay Message
	Route::get('/create_replay','replayMessage@index');
	Route::post('/insert','replayMessage@insert');
	Route::get('/all_replay','replayMessage@allreplay');
	Route::get('/edit_msq/{id}','replayMessage@edit')->name('edit_msq');
	Route::get('/edit_by_modal','replayMessage@edit_by_modal');
	Route::post('/updatereplay/{id}','replayMessage@update')->name('update_replay');
	Route::get('/delete_msg/{id}','replayMessage@delete')->name('delete_msq');


	// Followup
	Route::get('/create_followup','followup@index');
	Route::post('/insertfollowup','followup@insert');
	Route::get('/all_followup','followup@all');
	Route::get('/edit_followup/{id}','followup@edit')->name('edit_followup');
	Route::post('/updatefollowup/{id}','followup@update')->name('updatefollowup');
	Route::get('/delete_followup/{id}','followup@delete')->name('delete_followup');


});


// Administration
Route::group(['middleware'=>['adminAccess']],function(){
	//Blacklist
	Route::get('/blacklist','blacklistController@blacklist');
	Route::post('/addblacklist','blacklistController@add');
	Route::get('/edit_blacklist','blacklistController@edit');
	Route::post('/updateblacklist','blacklistController@update');
	Route::get('/delete_blacklist/{id}','blacklistController@delete')->name('delete_blacklist');

	//User management
	Route::get('/manageuser','userController@alluser');
	Route::post('/adduser','userController@insert');
	Route::get('/edit_user','userController@edituser');
	Route::post('/updateuser','userController@updateuser');

	//Reset System
	Route::get('/reset_system','resetController@reset_system');
	Route::post('/reset','resetController@resetsystem');

	//Lead management
	Route::get('/manageleads','leadController@manageleads');
	Route::post('/insertleads','leadController@insertleads');
	Route::post('/delete_leads','leadController@delete_leads');
	Route::get('/leadgenerate','leadController@leadgenerate');

	//Mail manager
	Route::get('/mail','mastermailController@mail');
	Route::post('/insertmail','mastermailController@insertmail');
	Route::get('/edit_mail','mastermailController@edit_mail');
	Route::post('/update_mail','mastermailController@update_mail');
	Route::get('/delete_mail/{id}','mastermailController@delete_mail')->name('delete_mail');


	//link manager
	Route::get('/linkmanager','linkController@linkmanager');
	Route::post('/insertlink','linkController@insertlink');
	Route::get('/edit_link','linkController@edit_link');
	Route::post('/update_link','linkController@update_link');
	Route::get('/delete_link/{id}','linkController@delete_link')->name('delete_link');
	
	// SMTP
	Route::get('/smtp','smtpController@index');
	Route::post('/updateact1','smtpController@updateact1');
	Route::post('/delete_smtp','smtpController@delete_smtp');
	Route::post('/uploadcsv','smtpController@uploadcsv');
	Route::post('/inactivegroup1','smtpController@inactivegroup1');

	//Conversation
	Route::get('/sendmail','PhpmailerController@sendEmail');
	Route::get('/overflow','overflowController@sendEmail');

});
Route::get('/etrack/{email}/{id}','PhpmailerController@etrack')->name('etrack');
Route::get('/logout', function () {
	Session::flush();
	return redirect('/admin')->with('status', 'You are looged out...!');
});